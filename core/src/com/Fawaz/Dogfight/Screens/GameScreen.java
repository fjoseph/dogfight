package com.Fawaz.Dogfight.Screens;

import com.Fawaz.Dogfight.Dogfight;
import com.Fawaz.Dogfight.View.GameWorld;
import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.Screen;
import com.badlogic.gdx.graphics.GL20;

public class GameScreen implements Screen {

	GameWorld world;
	public GameScreen(Dogfight game) {
		// TODO Auto-generated constructor stub
		world = new GameWorld(game);
		
	}

	@Override
	public void render(float delta) {
		// TODO Auto-generated method stub
		Gdx.gl.glClearColor(0.5f, 0.8f, 1, 1);
		Gdx.gl.glClear(GL20.GL_COLOR_BUFFER_BIT);
		world.update(delta);

	}

	@Override
	public void resize(int width, int height) {
		world.resize(width, height);
	}

	@Override
	public void show() {
		world.initializeWorld(true);

	}

	@Override
	public void hide() {
		world.freeCloudPool();
	}

	@Override
	public void pause() {
		// TODO Auto-generated method stub

	}

	@Override
	public void resume() {
		// TODO Auto-generated method stub

	}

	@Override
	public void dispose() {
		world.dispose();

	}

}
