package com.Fawaz.Dogfight.Screens;

import com.Fawaz.Dogfight.Dogfight;
import com.Fawaz.Dogfight.Utils.ResourceFactory;
import com.Fawaz.Dogfight.View.MainMenuWorld;
import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.Screen;
import com.badlogic.gdx.graphics.GL20;

public class MainMenu implements Screen {

	private MainMenuWorld world;

	public MainMenu(Dogfight game) {
		world = new MainMenuWorld(game);
	}

	@Override
	public void render(float delta) {
		Gdx.gl.glClearColor(0.5f, 0.8f, 1, 1);
		Gdx.gl.glClear(GL20.GL_COLOR_BUFFER_BIT);
		world.update(delta);
	}

	@Override
	public void resize(int width, int height) {
		world.resize(width, height);
	}

	@Override
	public void show() {
		ResourceFactory.playMainMenuMusic(true);
		ResourceFactory.playGameOverMusic(false);
		world.init();

	}

	@Override
	public void hide() {
		ResourceFactory.playMainMenuMusic(false);
		world.freeCloudPool();
	}

	@Override
	public void pause() {
		// TODO Auto-generated method stub

	}

	@Override
	public void resume() {
		ResourceFactory.playMainMenuMusic(true);

	}

	@Override
	public void dispose() {
		world.dispose();
	}

}
