package com.Fawaz.Dogfight;

import com.Fawaz.Dogfight.Screens.GameScreen;
import com.Fawaz.Dogfight.Screens.MainMenu;
import com.Fawaz.Dogfight.Utils.ResourceFactory;
import com.Fawaz.Dogfight.Utils.ThirdPartyInterface;
import com.badlogic.gdx.Application.ApplicationType;
import com.badlogic.gdx.Game;
import com.badlogic.gdx.Gdx;

public class Dogfight extends Game {

	private ResourceFactory resourceFactory;
	private GameScreen gameScreen;
	private MainMenu menu;
	private ThirdPartyInterface thirdParyInterface;

	public Dogfight(ThirdPartyInterface t) {
		thirdParyInterface = t;
	}

	@Override
	public void create() {
		resourceFactory = new ResourceFactory();
		gameScreen = new GameScreen(this);
		menu = new MainMenu(this);
		setScreen(menu);
		if (Gdx.app.getType() == ApplicationType.iOS)
			thirdParyInterface.signIn();
	}

	public void chooseScreens(int index) {

		switch (index) {
		case 1:
			setScreen(menu);
			break;
		case 2:
			setScreen(gameScreen);
			break;
		}
	}

	@Override
	public void dispose() {
		// TODO Auto-generated method stub
		super.dispose();
		resourceFactory.dispose();
		menu.dispose();
		gameScreen.dispose();
	}

	public ResourceFactory getResourceFactory() {
		return resourceFactory;
	}

	public void playAds() {
		thirdParyInterface.playAds();
	}

	public void unlockAchievements(int index) {
		thirdParyInterface.unlockAchievements(index);
	}

	public void submitScore(int destroyedPlanes, int score) {
		thirdParyInterface.submitScore(destroyedPlanes, score);
	}

	public void showAchievements() {
		thirdParyInterface.showAchievements();
	}

	public void showLeaderBoards() {
		thirdParyInterface.showLeaderBoards();
	}

}
