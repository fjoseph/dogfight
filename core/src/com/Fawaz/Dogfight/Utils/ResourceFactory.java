package com.Fawaz.Dogfight.Utils;

import com.Fawaz.Dogfight.Model.Hero;
import com.Fawaz.Dogfight.Model.Objects.Cloud;
import com.Fawaz.Dogfight.Model.Objects.Missile;
import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.audio.Music;
import com.badlogic.gdx.audio.Sound;
import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.graphics.OrthographicCamera;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.Texture.TextureFilter;
import com.badlogic.gdx.graphics.g2d.BitmapFont;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.graphics.g2d.TextureAtlas;
import com.badlogic.gdx.graphics.g2d.TextureAtlas.AtlasRegion;
import com.badlogic.gdx.graphics.g2d.freetype.FreeTypeFontGenerator;
import com.badlogic.gdx.graphics.g2d.freetype.FreeTypeFontGenerator.FreeTypeFontParameter;
import com.badlogic.gdx.scenes.scene2d.Stage;
import com.badlogic.gdx.scenes.scene2d.ui.Button.ButtonStyle;
import com.badlogic.gdx.scenes.scene2d.ui.Label.LabelStyle;
import com.badlogic.gdx.scenes.scene2d.ui.Skin;
import com.badlogic.gdx.scenes.scene2d.ui.TextButton.TextButtonStyle;
import com.badlogic.gdx.scenes.scene2d.ui.Touchpad.TouchpadStyle;
import com.badlogic.gdx.scenes.scene2d.ui.Window.WindowStyle;
import com.badlogic.gdx.utils.Array;
import com.badlogic.gdx.utils.Pool;
import com.badlogic.gdx.utils.viewport.StretchViewport;

public class ResourceFactory {

	public static float BOXTOWORLD = 64;
	public static float WORLDTOBOX = 0.015625f;
	private Array<TextureAtlas> enemyAtlas;
	private Array<AtlasRegion> explosionframes, missileFrames, playerHands,
			opponentHands, icons, scoreListing;
	private Texture cloudTexture, circle;
	private BitmapFont scoreFont;
	private Skin dialogSkin, uiSkin;
	private SpriteBatch batch;
	private static Array<Sound> soundArray;
	private static Music opening, gameOver;
	private Stage stage;
	private OrthographicCamera camera;
	private Pool<Cloud> cloudPool;

	public ResourceFactory() {

		camera = new OrthographicCamera(2500, 1500);
		batch = new SpriteBatch();
		stage = new Stage(new StretchViewport(2500, 1500));
		enemyAtlas = new Array<TextureAtlas>(5);
		for (int i = 1; i <= 5; i++) {
			enemyAtlas
					.add(new TextureAtlas("data/Enemies/Enemy" + i + ".pack"));
		}
		explosionframes = new TextureAtlas("data/Explosion.pack")
				.findRegions("Explosion");
		missileFrames = new TextureAtlas("data/Missile.pack")
				.findRegions("Missile");
		scoreListing = new TextureAtlas("data/Enemies/ScoreAmount.pack")
				.findRegions("up");

		cloudTexture = new Texture("data/Cloud.png");
		cloudTexture.setFilter(TextureFilter.Linear, TextureFilter.Linear);

		cloudPool = new Pool<Cloud>(10) {
			@Override
			protected Cloud newObject() {
				return new Cloud();
			}
		};

		circle = new Texture("data/MicroGame/Circle.png");
		circle.setFilter(TextureFilter.Linear, TextureFilter.Linear);

		TextureAtlas rps = new TextureAtlas("data/MicroGame/RPS/RPS.pack");
		playerHands = rps.findRegions("player");
		opponentHands = rps.findRegions("enemy");
		icons = rps.findRegions("icon");

		FreeTypeFontParameter parameter = new FreeTypeFontParameter();
		parameter.size = 46;
		FreeTypeFontGenerator generator = new FreeTypeFontGenerator(
				Gdx.files.internal("data/font.ttf"));
		BitmapFont dialogFont = generator.generateFont(parameter);
		scoreFont = generator.generateFont(parameter);
		scoreFont.setColor(Color.BLACK);
		scoreFont.setScale(.9f);
		generator.dispose();

		dialogSkin = new Skin(new TextureAtlas("data/Dialog/Dialog.pack"));

		WindowStyle ws = new WindowStyle(dialogFont, Color.WHITE,
				dialogSkin.getDrawable("dWindow"));
		dialogSkin.add("default", ws);
		LabelStyle ls = new LabelStyle(dialogFont, Color.valueOf("87ceeb"));
		dialogSkin.add("default", ls);
		TextButtonStyle tbs = new TextButtonStyle(
				dialogSkin.getDrawable("ButtonUp"),
				dialogSkin.getDrawable("ButtonDown"), null, dialogFont);
		tbs.fontColor = Color.WHITE;
		dialogSkin.add("default", tbs);
		ls = new LabelStyle(dialogFont, Color.WHITE);
		dialogSkin.add("MicroGame Title", ls);
		ls = new LabelStyle(dialogFont, Color.BLACK);
		dialogSkin.add("Score Label", ls);

		uiSkin = new Skin(new TextureAtlas("data/UI.pack"));

		TouchpadStyle touchStyle = new TouchpadStyle(
				uiSkin.getDrawable("background"),
				uiSkin.getDrawable("touchpad"));
		uiSkin.add("default", touchStyle);

		ButtonStyle fireButtonStyle = new ButtonStyle(
				uiSkin.getDrawable("fire"), uiSkin.getDrawable("fireDown"),
				null);
		uiSkin.add("fireButton", fireButtonStyle);

		ButtonStyle accelerateButtonStyle = new ButtonStyle(
				uiSkin.getDrawable("stop"), null, uiSkin.getDrawable("start"));
		uiSkin.add("accelerateButton", accelerateButtonStyle);

		TextButtonStyle ts = new TextButtonStyle(
				uiSkin.getDrawable("ButtonUp"),
				uiSkin.getDrawable("ButtonDown"), null, dialogFont);
		ts.fontColor = Color.BLACK;
		ButtonStyle acheivementStyle = new ButtonStyle(
				uiSkin.getDrawable("Aup"), uiSkin.getDrawable("Adown"), null);
		ButtonStyle leaderBoardStyle = new ButtonStyle(
				uiSkin.getDrawable("Lup"), uiSkin.getDrawable("Ldown"), null);
		uiSkin.add("default", ts);
		uiSkin.add("Ach", acheivementStyle);
		uiSkin.add("Lea", leaderBoardStyle);

		soundArray = new Array<Sound>(10);

		soundArray.add(Gdx.audio.newSound(Gdx.files
				.internal("data/Sound/142608__autistic-lucario__error.wav")));
		soundArray.add(Gdx.audio.newSound(Gdx.files
				.internal("data/Sound/162472__kastenfrosch__scary.wav")));
		soundArray.add(Gdx.audio.newSound(Gdx.files
				.internal("data/Sound/76234__jivatma07__level-up-3note2.wav")));
		soundArray
				.add(Gdx.audio.newSound(Gdx.files
						.internal("data/Sound/202530__kalisemorrison__scanner-beep.wav")));
		soundArray
				.add(Gdx.audio.newSound(Gdx.files
						.internal("data/Sound/180005__gentlemanwalrus__shocked-gasp.wav")));
		soundArray
				.add(Gdx.audio.newSound(Gdx.files
						.internal("data/Sound/69439__guitarguy1985__synth-cricket.wav")));
		soundArray.add(Gdx.audio.newSound(Gdx.files
				.internal("data/Sound/accelerate.wav")));
		soundArray.add(Gdx.audio.newSound(Gdx.files
				.internal("data/Sound/explosion.wav")));
		soundArray.add(Gdx.audio.newSound(Gdx.files
				.internal("data/Sound/missile.wav")));
		soundArray.add(Gdx.audio.newSound(Gdx.files
				.internal("data/Sound/windingdown.wav")));
		soundArray.add(Gdx.audio.newSound(Gdx.files
				.internal("data/Sound/click1.ogg")));
		soundArray.add(Gdx.audio.newSound(Gdx.files
				.internal("data/Sound/rollover1.ogg")));

		gameOver = Gdx.audio.newMusic(Gdx.files
				.internal("data/Sound/sad_Exploring.mp3"));
		opening = Gdx.audio.newMusic(Gdx.files
				.internal("data/Sound/theme_of_the_defenders.mp3"));
		gameOver.setLooping(true);
		opening.setLooping(true);

		Missile.frames = missileFrames;
		Cloud.texture = cloudTexture;
		Gdx.input.setInputProcessor(stage);

	}

	public Array<AtlasRegion> getExplosionframes() {
		return explosionframes;
	}

	public TextureAtlas getTextureAtlas(int i) {
		return enemyAtlas.get(i);
	}

	public Array<AtlasRegion> getMissileframes() {
		return missileFrames;
	}

	public static float distance(float deltaX, float deltaY) {
		return (float) Math.sqrt(Math.pow(deltaX, 2) + Math.pow(deltaY, 2));

	}

	public Skin getDialogSkin() {
		return dialogSkin;
	}

	public Skin getuiSkin() {
		return uiSkin;
	}

	public Array<AtlasRegion> getScoreListing() {
		return scoreListing;
	}

	public Array<AtlasRegion> getPlayerHands() {
		return playerHands;
	}

	public Array<AtlasRegion> getOpponentHands() {
		return opponentHands;
	}

	public Array<AtlasRegion> getIcons() {
		return icons;
	}

	public Texture getCircle() {
		return circle;
	}

	public LabelStyle getScoreStyle() {
		return dialogSkin.get("Score Label", LabelStyle.class);
	}

	public SpriteBatch getBatch() {
		return batch;
	}

	public Pool<Cloud> getCloudPool() {
		return cloudPool;
	}

	public BitmapFont getScoreFont() {
		return scoreFont;
	}

	public Stage getStage() {
		return stage;
	}

	public OrthographicCamera getCamera() {
		return camera;
	}

	public static void playSound(int index) {
		if (index == 7)
			soundArray.get(index).play(.1f);
		else
			soundArray.get(index).play(.4f);
	}

	public static void playGameOverMusic(boolean isPlay) {
		if (isPlay)
			gameOver.play();
		else
			gameOver.stop();
	}

	public static void playMainMenuMusic(boolean isPlay) {
		if (isPlay)
			opening.play();
		else
			opening.stop();
	}

	public void dispose() {

		for (Sound sound : soundArray)
			sound.dispose();
		for (TextureAtlas atlas : enemyAtlas)
			atlas.dispose();
		for (AtlasRegion region : explosionframes)
			region.getTexture().dispose();
		for (AtlasRegion region : missileFrames)
			region.getTexture().dispose();
		for (AtlasRegion region : missileFrames)
			region.getTexture().dispose();
		for (AtlasRegion region : playerHands)
			region.getTexture().dispose();
		for (AtlasRegion region : opponentHands)
			region.getTexture().dispose();
		for (AtlasRegion region : icons)
			region.getTexture().dispose();
		for (AtlasRegion region : scoreListing)
			region.getTexture().dispose();
		cloudTexture.dispose();
		circle.dispose();
		dialogSkin.dispose();
		uiSkin.dispose();
		scoreFont.dispose();
		opening.dispose();
		gameOver.dispose();
		batch.dispose();
		stage.dispose();
		Hero.dispose();
	}
}
