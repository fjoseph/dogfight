package com.Fawaz.Dogfight.Utils;

import com.Fawaz.Dogfight.Model.Plane;

public interface MissileCreator {
	public void createMissile(Plane plane);
}
