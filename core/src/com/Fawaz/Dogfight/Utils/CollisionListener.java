package com.Fawaz.Dogfight.Utils;

import com.Fawaz.Dogfight.Model.Hero;
import com.Fawaz.Dogfight.Model.Plane;
import com.Fawaz.Dogfight.Model.Objects.Explosion;
import com.Fawaz.Dogfight.Model.Objects.Missile;
import com.badlogic.gdx.graphics.g2d.TextureAtlas.AtlasRegion;
import com.badlogic.gdx.physics.box2d.Body;
import com.badlogic.gdx.physics.box2d.Contact;
import com.badlogic.gdx.physics.box2d.ContactImpulse;
import com.badlogic.gdx.physics.box2d.ContactListener;
import com.badlogic.gdx.physics.box2d.Manifold;
import com.badlogic.gdx.utils.Array;

public class CollisionListener implements ContactListener {
	private CollisionCallBack modelWorld;
	private Array<AtlasRegion> explosionFrames;

	public CollisionListener(Array<AtlasRegion> explosionFrames,
			CollisionCallBack modelWorld) {
		// TODO Auto-generated constructor stub
		this.explosionFrames = explosionFrames;
		this.modelWorld = modelWorld;
	}

	@Override
	public void beginContact(Contact contact) {
		// TODO Auto-generated method stub
		Body bodyA, bodyB;
		Plane plane;
		Missile missile = null;
		bodyA = contact.getFixtureA().getBody();
		bodyB = contact.getFixtureB().getBody();

		if (bodyA.getUserData() == null || bodyB.getUserData() == null)
			return;

		if (bodyA.getUserData() instanceof Plane) {
			plane = (Plane) bodyA.getUserData();
			modelWorld.removeFromRegistry(plane);
			if (plane instanceof Hero)
				createExplosion(plane.getX(), plane.getY(), plane.getWidth(),
						plane.getHeight(), plane.getRotation(), true);
			else
				createExplosion(plane.getX(), plane.getY(), plane.getWidth(),
						plane.getHeight(), plane.getRotation(), false);

		}

		if (bodyB.getUserData() instanceof Plane) {
			plane = (Plane) bodyB.getUserData();
			modelWorld.removeFromRegistry(plane);
			if (plane instanceof Hero)
				createExplosion(plane.getX(), plane.getY(), plane.getWidth(),
						plane.getHeight(), plane.getRotation(), true);
			else
				createExplosion(plane.getX(), plane.getY(), plane.getWidth(),
						plane.getHeight(), plane.getRotation(), false);
		}
		if (bodyA.getUserData() instanceof Missile) {
			missile = (Missile) bodyA.getUserData();
			modelWorld.removeFromRegistry(missile);
		}

		if (bodyB.getUserData() instanceof Missile) {
			missile = (Missile) bodyB.getUserData();
			modelWorld.removeFromRegistry(missile);
		}

		if (bodyA.getUserData() instanceof Missile
				&& bodyB.getUserData() instanceof Missile) {
			// Missile is assigned to bodyB if the if-statement is true
			createExplosion(missile.getX(), missile.getY(), 70, 70,
					missile.getRotation(), false);

		}

		modelWorld.scheduleForRemoval((Disposable) bodyA.getUserData());
		modelWorld.scheduleForRemoval((Disposable) bodyB.getUserData());

	}

	@Override
	public void endContact(Contact contact) {
		// TODO Auto-generated method stub

	}

	@Override
	public void preSolve(Contact contact, Manifold oldManifold) {
		// TODO Auto-generated method stub

	}

	@Override
	public void postSolve(Contact contact, ContactImpulse impulse) {
		// TODO Auto-generated method stub

	}

	public void createExplosion(float x, float y, float width, float height,
			float rotation, boolean isHero) {
		Explosion explosion = new Explosion(explosionFrames);
		explosion.setBounds(x, y, width, height);
		explosion.setOrigin(width / 2, height / 2);
		explosion.setHero(isHero);
		explosion.setRotation(rotation);
		modelWorld.addToRegistry(explosion);
	}

}
