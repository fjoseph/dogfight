package com.Fawaz.Dogfight.Utils;

import com.Fawaz.Dogfight.Model.Objects.Explosion;

public interface CollisionCallBack {

	public void scheduleForRemoval(Disposable object);
	
	public void addToRegistry(Explosion explosion);
	
	public void removeFromRegistry(Object object);


}
