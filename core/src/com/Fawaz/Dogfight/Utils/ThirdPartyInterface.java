package com.Fawaz.Dogfight.Utils;

public interface ThirdPartyInterface {

	public void playAds();

	public void unlockAchievements(int index);

	public void showAchievements();

	public void showLeaderBoards();

	public void submitScore(int index, int score);

	public void signIn();

	public boolean isSignedIn();

}
