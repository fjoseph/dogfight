package com.Fawaz.Dogfight.Utils;

import com.badlogic.gdx.physics.box2d.Body;

public interface Disposable {

	public Body getBody();
}
