package com.Fawaz.Dogfight.View;

import com.Fawaz.Dogfight.Dogfight;
import com.Fawaz.Dogfight.Model.Enemy;
import com.Fawaz.Dogfight.Model.Hero;
import com.Fawaz.Dogfight.Model.Plane;
import com.Fawaz.Dogfight.Model.Objects.Cloud;
import com.Fawaz.Dogfight.Model.Objects.Explosion;
import com.Fawaz.Dogfight.Model.Objects.MicroGameWindow;
import com.Fawaz.Dogfight.Model.Objects.Missile;
import com.Fawaz.Dogfight.Model.Objects.ScoreBoard;
import com.Fawaz.Dogfight.Model.Objects.ScoreInformation;
import com.Fawaz.Dogfight.Model.Objects.MicroGames.MicroGame;
import com.Fawaz.Dogfight.Model.Objects.MicroGames.ShellGame;
import com.Fawaz.Dogfight.Model.Objects.MicroGames.RPS.RockPaperScissors;
import com.Fawaz.Dogfight.Utils.CollisionCallBack;
import com.Fawaz.Dogfight.Utils.CollisionListener;
import com.Fawaz.Dogfight.Utils.Disposable;
import com.Fawaz.Dogfight.Utils.MissileCreator;
import com.Fawaz.Dogfight.Utils.ResourceFactory;
import com.Fawaz.Dogfight.Utils.State;
import com.badlogic.gdx.Application;
import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.Preferences;
import com.badlogic.gdx.graphics.FPSLogger;
import com.badlogic.gdx.graphics.OrthographicCamera;
import com.badlogic.gdx.graphics.g2d.BitmapFont;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.graphics.g2d.TextureAtlas.AtlasRegion;
import com.badlogic.gdx.math.MathUtils;
import com.badlogic.gdx.math.Vector2;
import com.badlogic.gdx.physics.box2d.World;
import com.badlogic.gdx.scenes.scene2d.Actor;
import com.badlogic.gdx.scenes.scene2d.InputEvent;
import com.badlogic.gdx.scenes.scene2d.InputListener;
import com.badlogic.gdx.scenes.scene2d.Stage;
import com.badlogic.gdx.scenes.scene2d.actions.Actions;
import com.badlogic.gdx.scenes.scene2d.ui.Button;
import com.badlogic.gdx.scenes.scene2d.ui.Button.ButtonStyle;
import com.badlogic.gdx.scenes.scene2d.ui.Dialog;
import com.badlogic.gdx.scenes.scene2d.ui.Image;
import com.badlogic.gdx.scenes.scene2d.ui.Label;
import com.badlogic.gdx.scenes.scene2d.ui.Label.LabelStyle;
import com.badlogic.gdx.scenes.scene2d.ui.Skin;
import com.badlogic.gdx.scenes.scene2d.ui.TextButton;
import com.badlogic.gdx.scenes.scene2d.ui.Touchpad;
import com.badlogic.gdx.scenes.scene2d.utils.ChangeListener;
import com.badlogic.gdx.utils.Array;
import com.badlogic.gdx.utils.Pool;
import com.badlogic.gdx.utils.Timer;
import com.badlogic.gdx.utils.Timer.Task;

public class GameWorld implements MissileCreator, CollisionCallBack {

	private Dogfight game;
	private Stage stage;
	private Hero heroPlane;
	private Touchpad touchpad;
	private Button accelerate, fire;
	private World world;
	private FPSLogger logger;
	private Array<Missile> missileRegistry;
	private Array<Enemy> enemyRegistry;
	private Array<Explosion> explosionRegistry;
	private Array<Disposable> garbageDisposal;
	private Array<Cloud> cloudRegistry;
	private Array<MicroGame> microgameArray;
	private Array<ScoreInformation> scoreInformationRegistry;
	private Array<Image> scoreListing;
	private Array<Label> scoreLabels;
	private ResourceFactory resourceFactory;
	private Task populateEnemyTask;
	private State state;
	private Dialog dialog;
	private MicroGameWindow mgWindow;
	private MicroGame currentMG;
	private OrthographicCamera camera;
	private SpriteBatch batch;
	private ScoreBoard scoreBoard;
	private BitmapFont font;
	private Preferences preference;
	private float timeAlive;
	private Label allowAds;
	private int timesWon, planeDestroyed;
	private Pool<Cloud> cloudPool;

	public GameWorld(Dogfight game) {
		this.game = game;
		resourceFactory = game.getResourceFactory();
		batch = resourceFactory.getBatch();
		camera = resourceFactory.getCamera();
		stage = resourceFactory.getStage();
		font = resourceFactory.getScoreFont();
		LabelStyle ls = resourceFactory.getScoreStyle();
		logger = new FPSLogger();
		world = new World(new Vector2(0, -2.5f), true);
		Skin skin = resourceFactory.getuiSkin();
		heroPlane = new Hero(world, this);
		touchpad = new Touchpad(2, skin);
		fire = new Button(skin.get("fireButton", ButtonStyle.class));
		accelerate = new Button(skin.get("accelerateButton", ButtonStyle.class));
		missileRegistry = new Array<Missile>();
		enemyRegistry = new Array<Enemy>();
		explosionRegistry = new Array<Explosion>();
		garbageDisposal = new Array<Disposable>();
		cloudRegistry = new Array<Cloud>();
		scoreInformationRegistry = new Array<ScoreInformation>();
		microgameArray = new Array<MicroGame>(2);
		scoreListing = new Array<Image>(5);
		scoreLabels = new Array<Label>(5);
		cloudPool = resourceFactory.getCloudPool();
		mgWindow = new MicroGameWindow(resourceFactory);
		preference = Gdx.app.getPreferences("Dogfight Gauntlet");
		scoreBoard = new ScoreBoard(ls, preference.getInteger("Score", 0));
		planeDestroyed = 0;
		timesWon = 0;

		int score = 80;
		for (int i = 0; i < 5; i++) {
			Image image = new Image(resourceFactory.getScoreListing().get(i));
			Label label = new Label(": " + score, ls);
			scoreListing.add(image);
			image.setSize(128, 64);
			image.setPosition(2000, 500 + (80 * i));
			scoreLabels.add(label);
			label.setPosition(2000 + 148, image.getY());
			score /= 2;
		}
		allowAds = new Label(
				"You have to play for 30 seconds for an ad to show"
						+ "\n when you lose a game of chance or stop playing",
				ls);

		initializeDialog();
		initializeTask();

		touchpad.addListener(new ChangeListener() {
			float rotation;

			@Override
			public void changed(ChangeEvent event, Actor actor) {
				if (actor.equals(touchpad)) {
					rotation = MathUtils.atan2(touchpad.getKnobPercentY(),
							touchpad.getKnobPercentX());
					while (rotation < 0)
						rotation += 6;

					if (touchpad.isTouched())
						heroPlane.setBox2dAngle(rotation);
				}
			}
		});
		fire.addListener(new InputListener() {
			@Override
			public boolean touchDown(InputEvent event, float x, float y,
					int pointer, int button) {
				heroPlane.fireMissile();
				return true;
			}

		});

		accelerate.addListener(new InputListener() {
			@Override
			public boolean touchDown(InputEvent event, float x, float y,
					int pointer, int button) {
				return true;
			}

			@Override
			public void touchUp(InputEvent event, float x, float y,
					int pointer, int button) {
				if (accelerate.isChecked())
					ResourceFactory.playSound(6);
				else
					ResourceFactory.playSound(9);
			}
		});

		world.setContactListener(new CollisionListener(resourceFactory
				.getExplosionframes(), this));
		microgameArray.add(new RockPaperScissors(this));
		microgameArray.add(new ShellGame(this, resourceFactory.getCircle()));
		Gdx.app.setLogLevel(Application.LOG_NONE);

		touchpad.setPosition(150, 50);
		accelerate.setPosition(1900, 100);
		fire.setPosition(2200, 100);
		scoreBoard.setPosition(0, 1500 - scoreBoard.getHeight());
	}

	private void initializeDialog() {
		Skin dialogSkin = resourceFactory.getDialogSkin();
		dialog = new Dialog("Chance for Redemption", dialogSkin) {
			@Override
			protected void result(Object object) {

				String string = object.toString();
				if (string.equals("Yes")) {
					currentMG = microgameArray.random();
					mgWindow.initializeMicroGame(currentMG);
					stage.addActor(mgWindow);
					allowAds.remove();
				} else {
					initializeScoreScreen(true);
					allowAds.remove();

				}

			}
		};
		Label label = new Label("Play a game of chance to continue?",
				dialogSkin);
		label.setWrap(true);
		TextButton affirmative = new TextButton("Yes", dialogSkin);
		TextButton negative = new TextButton("No", dialogSkin);

		affirmative.addListener(new InputListener() {
			@Override
			public boolean touchDown(InputEvent event, float x, float y,
					int pointer, int button) {
				ResourceFactory.playSound(10);
				return true;
			}

			@Override
			public void touchUp(InputEvent event, float x, float y,
					int pointer, int button) {
				ResourceFactory.playSound(11);
			}
		});
		negative.addListener(new InputListener() {
			@Override
			public boolean touchDown(InputEvent event, float x, float y,
					int pointer, int button) {
				ResourceFactory.playSound(10);
				return true;
			}

			@Override
			public void touchUp(InputEvent event, float x, float y,
					int pointer, int button) {
				ResourceFactory.playSound(11);
			}
		});

		dialog.pad(50, 70, 70, 70);
		dialog.getContentTable().pad(50, 70, 50, 70).add(label).width(600);
		dialog.getButtonTable().padTop(20).add(negative).width(200)
				.padRight(50);
		dialog.setObject(affirmative, "Yes");
		dialog.getButtonTable().add(affirmative).width(200).padLeft(50);
		dialog.setObject(negative, "No");
		dialog.invalidateHierarchy();
		dialog.invalidate();
		dialog.layout();

		// MicroGame Window
		TextButton restart = new TextButton("Restart", dialogSkin);
		TextButton mainMenu = new TextButton("Main menu", dialogSkin);
		restart.addListener(new InputListener() {
			@Override
			public boolean touchDown(InputEvent event, float x, float y,
					int pointer, int button) {
				return true;
			}

			@Override
			public void touchUp(InputEvent event, float x, float y,
					int pointer, int button) {
				// heroplane.setInitialPosition();
				cloudRegistry.clear();
				initializeWorld(true);
			}
		});

		mainMenu.addListener(new InputListener() {
			@Override
			public boolean touchDown(InputEvent event, float x, float y,
					int pointer, int button) {
				return true;
			}

			@Override
			public void touchUp(InputEvent event, float x, float y,
					int pointer, int button) {
				game.chooseScreens(1);
			}
		});

		restart.setSize(mainMenu.getPrefWidth() + 50, 150);
		mainMenu.setSize(mainMenu.getPrefWidth() + 50, 150);
		allowAds.setPosition(1250 - allowAds.getPrefWidth() * .5f, 1200);
		mgWindow.setButtons(restart, mainMenu);
	}

	private void initializeTask() {
		populateEnemyTask = new Task() {

			@Override
			public void run() {

				float x, y, chance;
				int spawnCount = MathUtils.random(1, 3);
				for (int i = 0; i < spawnCount; i++) {
					chance = 5;
					chance = (chance - enemyRegistry.size) / 10;
					if (MathUtils.randomBoolean(chance)) {
						int random = MathUtils.random(4);
						Enemy enemy = new Enemy(world,
								resourceFactory.getTextureAtlas(MathUtils
										.random(random)), GameWorld.this);
						enemy.setLevel(random);
						switch (MathUtils.random(3)) {
						case 0:
							// right side
							x = heroPlane.getX() + 1500;
							y = heroPlane.getY() - 750;
							enemy.setBearing(MathUtils.random(x, x + 700),
									MathUtils.random(y, y + 1500), 180);
							break;
						case 1:
							// left
							x = heroPlane.getX() - 1700;
							y = heroPlane.getY() - 750;
							enemy.setBearing(MathUtils.random(x - 500, x),
									MathUtils.random(y, y + 750), 0);
							break;
						case 2:
							// down
							x = heroPlane.getX() - 1250;
							y = heroPlane.getY() - 1500;
							enemy.setBearing(MathUtils.random(x, x + 2500),
									MathUtils.random(y, y + 500), 90);
							break;
						case 3:
							// up
							x = heroPlane.getX() - 1250;
							y = heroPlane.getY() + 1500;
							enemy.setBearing(MathUtils.random(x, x + 2500),
									MathUtils.random(y, y + 500), 270);
							break;
						}

						enemyRegistry.add(enemy);
					}
				}

			}
		};

	}

	public void wonGame(int index) {
		timesWon++;
		game.unlockAchievements(2);
		if (timesWon >= 3)
			game.unlockAchievements(3);
		switch (index) {
		case 0:
			game.unlockAchievements(4);
			break;
		case 1:
			game.unlockAchievements(5);
			break;
		}
		initializeWorld(false);
	}

	public void initializeWorld(boolean restart) {
		// stage.addActor(heroplane);
		Timer.schedule(populateEnemyTask, 1, .5f);
		state = State.INGAME;
		mgWindow.remove();
		stage.clear();
		stage.addActor(touchpad);
		stage.addActor(accelerate);
		stage.addActor(fire);
		stage.addActor(scoreBoard);

		for (int i = 0; i < 5; i++) {
			Image image = scoreListing.get(i);
			Label label = scoreLabels.get(i);
			image.addAction(Actions.delay(3, Actions.removeActor()));
			label.addAction(Actions.delay(3, Actions.removeActor()));
			stage.addActor(label);
			stage.addActor(image);
		}

		if (restart) {
			scoreBoard.init();
			timeAlive = 0;
		}
		ResourceFactory.playGameOverMusic(false);
		ResourceFactory.playMainMenuMusic(false);
	}

	public void update(float delta) {
		logger.log();
		switch (state) {
		case DIED:
			updateDialogWorld(delta);
			break;
		case INGAME:
			updateWorld(delta);
			world.step(1 / 45f, 6, 2);
			disposeGarbage();
			break;
		case PAUSE:
			break;

		}
		stage.act();
		stage.draw();

	}

	public void resize(int width, int height) {
		stage.getViewport().update(width, height, true);
	}

	private void updateWorld(float delta) {
		float x, y, distance;
		x = heroPlane.getX();
		y = heroPlane.getY();
		timeAlive += delta;
		heroPlane.update(accelerate.isChecked(), delta);
		camera.position.set(x + 64, y + 32, 0);
		camera.update();
		batch.setProjectionMatrix(camera.combined);
		populateClouds();

		batch.begin();
		for (Cloud cloud : cloudRegistry) {
			cloud.update(delta);
			float cloudX = cloud.getX(), cloudY = cloud.getY();
			// Is the cloud still on screen
			cloudX -= x;
			cloudY -= y;
			distance = ResourceFactory.distance(cloudX, cloudY);
			if (distance > 1724)
				cloud.updateTimeNotSeen(delta);
			else
				cloud.updateTimeNotSeen(-1);

			if (cloud.Overdue()) {
				cloudRegistry.removeValue(cloud, false);
				cloudPool.free(cloud);
			} else
				cloud.draw(batch);

		}

		for (ScoreInformation score : scoreInformationRegistry) {

			if (score.update(delta))
				scoreInformationRegistry.removeValue(score, false);
			else
				font.draw(batch, score.score, score.x, score.y);

		}

		for (Missile missile : missileRegistry) {
			float missileX = missile.getX(), missileY = missile.getY();
			missile.update(delta);
			// Is the missile still on screen
			missileX = missileX - x;
			missileY = missileY - y;
			distance = ResourceFactory.distance(missileX, missileY);
			// half width and height b/c plane is in the middle of ship
			if (distance > 1724)
				missile.updateTimeNotSeen(delta);
			else
				missile.updateTimeNotSeen(-1);

			if (missile.Overdue()) {
				world.destroyBody(missile.getBody());// Destroy body
				missileRegistry.removeValue(missile, false);
				// Remove from registry
			} else
				missile.draw(batch);
		}

		for (Enemy enemy : enemyRegistry) {

			float deltaX, deltaY, enemyX = enemy.getX(), enemyY = enemy.getY();
			deltaX = x - enemyX;
			deltaY = y - enemyY;
			distance = ResourceFactory.distance(deltaX, deltaY);
			// Get the angle between the two
			enemy.setBox2dAngle(MathUtils.atan2(deltaY, deltaX));
			enemy.accelerate(delta);// Move the box2d object
			enemy.update(delta);// Update the Image
			enemy.decisionToFire(distance);

			if (distance > 1724)
				enemy.updateTimeNotSeen(delta);
			else
				enemy.updateTimeNotSeen(-1);

			if (enemy.Overdue()) {
				world.destroyBody(enemy.getBody());// Destroy body
				enemyRegistry.removeValue(enemy, false);
				// Remove from registry
			} else
				enemy.draw(batch);

		}

		for (Explosion explosion : explosionRegistry) {
			if (explosion.update(delta))
				explosionRegistry.removeValue(explosion, false);
			else
				explosion.draw(batch);

		}

		scoreBoard.update(delta);

		heroPlane.draw(batch);
		batch.end();

	}

	private void updateDialogWorld(float delta) {
		populateClouds();
		batch.begin();
		for (Cloud cloud : cloudRegistry) {
			float cloudDistance;
			cloud.update(delta);
			float cloudX = cloud.getX(), cloudY = cloud.getY();
			// Is the cloud still on screen
			cloudX = heroPlane.getX() - cloudX;
			cloudY = heroPlane.getY() - cloudY;
			cloudDistance = ResourceFactory.distance(cloudX, cloudY);
			// half width and height b/c plane is in the middle of ship
			if (cloudDistance > 2000)
				cloud.updateTimeNotSeen(delta);
			else
				cloud.updateTimeNotSeen(-1);

			if (cloud.Overdue()) {
				cloudRegistry.removeValue(cloud, false);
				cloudPool.free(cloud);
			} else
				cloud.draw(batch);
		}

		for (ScoreInformation score : scoreInformationRegistry) {

			if (score.update(delta))
				scoreInformationRegistry.removeValue(score, false);
			else
				font.draw(batch, score.score, score.x, score.y);

		}

		for (Explosion explosion : explosionRegistry) {
			if (explosion.update(delta)) {
				explosionRegistry.removeValue(explosion, false);
				if (explosion.isHero())
					startRedemption();
			} else
				explosion.draw(batch);
		}

		batch.end();

	}

	private void populateClouds() {
		float chance = (20 - cloudRegistry.size) / 10;
		float x, y;
		x = heroPlane.getX();
		y = heroPlane.getY();

		if (MathUtils.randomBoolean(chance)) {
			Cloud cloud = cloudPool.obtain();
			cloud.init();
			if (state == State.INGAME)
				switch (MathUtils.random(3)) {
				case 0: // up
					cloud.setPosition(MathUtils.random(x - 1500, x + 1500),
							MathUtils.random(y + 1000, y + 1500));
					break;
				case 1: // down
					cloud.setPosition(MathUtils.random(x - 1500, x + 1500),
							MathUtils.random(y - 1500, y - 1000));
					break;
				case 2: // left
					cloud.setPosition(MathUtils.random(x - 2500, x - 1500),
							MathUtils.random(y - 1000, y + 1000));
					break;
				case 3: // right
					cloud.setPosition(MathUtils.random(x + 1500, x + 2500),
							MathUtils.random(y - 1000, y + 1000));
					break;

				}
			else
				switch (MathUtils.random(1)) {

				case 0: // left
					cloud.setPosition(MathUtils.random(x - 2500, x - 1500),
							MathUtils.random(y - 1000, y + 1000));
					cloud.setDirection(true);
					break;
				case 1: // right
					cloud.setPosition(MathUtils.random(x + 1500, x + 2500),
							MathUtils.random(y - 1000, y + 1000));
					cloud.setDirection(false);
					break;
				}
			cloudRegistry.add(cloud);
		}

	}

	private void startRedemption() {
		dialog.show(stage);
		for (Enemy enemy : enemyRegistry)
			world.destroyBody(enemy.getBody());
		for (Missile missile : missileRegistry)
			world.destroyBody(missile.getBody());
		if (timeAlive < 30) {
			stage.addActor(allowAds);
		}
		enemyRegistry.clear();
		missileRegistry.clear();
		touchpad.remove();
		fire.remove();
		accelerate.remove();

	}

	private void disposeGarbage() {
		for (Disposable dispose : garbageDisposal) {
			world.destroyBody(dispose.getBody());
		}
		garbageDisposal.clear();
	}

	@Override
	public void createMissile(Plane plane) {
		float rotation = plane.getRotation(), radian, x, y;
		Missile missile = new Missile(world, plane.returnGroupIndex());
		if (plane instanceof Hero)
			ResourceFactory.playSound(8);
		if ((rotation <= 45 || rotation >= 315)) {
			radian = rotation * MathUtils.degreesToRadians;
			x = plane.getX() + 40;
			y = plane.getY();

			// r cos t
			x = x + 15 * MathUtils.cos(radian);
			y = y + 10 * MathUtils.sin(radian);

			x = (plane.getX() + 40) + 25 * MathUtils.cos(radian);
			y = (plane.getY()) + 25 * MathUtils.sin(radian);
			missile.initialPositon(x, y, rotation);

		} else if (rotation > 45 && rotation < 135) {
			radian = rotation * MathUtils.degreesToRadians;
			x = plane.getX() + 32;
			y = plane.getY() + 64;

			// r cos t
			x = x + 67 * MathUtils.cos(radian);
			y = y + 32 * MathUtils.sin(radian);
			missile.initialPositon(x, y, rotation);
		} else if (rotation > 135 && rotation < 225) {

			radian = rotation * MathUtils.degreesToRadians;
			x = plane.getX();
			y = plane.getY();

			// r cos t

			x = x + 25 * MathUtils.cos(radian);
			y = y + 25 * MathUtils.sin(radian);
			missile.initialPositon(x, y, rotation);

		} else if (rotation > 225 && rotation < 315) {

			radian = rotation * MathUtils.degreesToRadians;
			x = plane.getX() + 32;
			y = plane.getY() - 64;

			// r cos t
			x = x + 67 * MathUtils.cos(radian);
			y = y - 32 * MathUtils.sin(radian);
			missile.initialPositon(x, y, rotation);

		}
		missileRegistry.add(missile);

	}

	public void initializeScoreScreen(boolean isFromGame) {
		int score, combo, bestScore, bestCombo;
		float bestTimeAlive;
		timesWon = 0;
		score = scoreBoard.getScore();
		combo = scoreBoard.getHighestCombo();
		bestScore = scoreBoard.getBest();
		bestCombo = preference.getInteger("Combo", 0);
		bestTimeAlive = preference.getFloat("Time", 0);
		mgWindow.initializeScoreList(score, bestScore, timeAlive,
				bestTimeAlive, combo, bestCombo, isFromGame);
		stage.addActor(mgWindow);
		if (score > bestScore) {
			preference.putInteger("Score", score);
			preference.putInteger("Combo", combo);
			preference.putFloat("Time", timeAlive);
			preference.flush();
			scoreBoard.setBestScore(score);
			game.submitScore(planeDestroyed, score);

		}
		if (timeAlive > 30) {
			game.playAds();
		}
	}

	@Override
	public void scheduleForRemoval(Disposable disposable) {

		if (disposable instanceof Hero) {
			state = State.DIED;
			populateEnemyTask.cancel();
		} else if (!garbageDisposal.contains(disposable, false))
			garbageDisposal.add(disposable);
	}

	@Override
	public void addToRegistry(Explosion explosion) {
		explosionRegistry.add(explosion);
	}

	@Override
	public void removeFromRegistry(Object object) {
		if (object instanceof Enemy) {
			Enemy enemy = (Enemy) object;
			int updatedScore = scoreBoard.updateScore(enemy.getScore());
			scoreInformationRegistry.add(new ScoreInformation(enemy.getX(),
					enemy.getY() + 64, Integer.toString(updatedScore)));
			scoreInformationRegistry.add(new ScoreInformation(enemy.getX(),
					enemy.getY() + 64, "x" + scoreBoard.getCombo()));
			enemyRegistry.removeValue(enemy, false);
			if (++planeDestroyed == 1)
				game.unlockAchievements(1);
			if (scoreBoard.getCombo() >= 3)
				game.unlockAchievements(6);
		} else if (object instanceof Missile)
			missileRegistry.removeValue((Missile) object, false);
		ResourceFactory.playSound(7);
	}

	public Array<AtlasRegion> getMicroGameAsset(int i) {
		switch (i) {
		case 1:
			return resourceFactory.getPlayerHands();
		case 2:
			return resourceFactory.getOpponentHands();
		case 3:
			return resourceFactory.getIcons();
		default:
			return null;
		}

	}

	public void dispose() {
		world.dispose();
	}

	public void freeCloudPool() {
		cloudPool.freeAll(cloudRegistry);
		cloudRegistry.clear();
	}

}
