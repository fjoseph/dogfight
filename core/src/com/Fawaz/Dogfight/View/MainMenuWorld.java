package com.Fawaz.Dogfight.View;

import com.Fawaz.Dogfight.Dogfight;
import com.Fawaz.Dogfight.Model.Hero;
import com.Fawaz.Dogfight.Model.Plane;
import com.Fawaz.Dogfight.Model.Objects.Cloud;
import com.Fawaz.Dogfight.Model.Objects.Missile;
import com.Fawaz.Dogfight.Utils.MissileCreator;
import com.Fawaz.Dogfight.Utils.ResourceFactory;
import com.badlogic.gdx.Application.ApplicationType;
import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.OrthographicCamera;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.Texture.TextureFilter;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.math.MathUtils;
import com.badlogic.gdx.math.Vector2;
import com.badlogic.gdx.physics.box2d.World;
import com.badlogic.gdx.scenes.scene2d.Actor;
import com.badlogic.gdx.scenes.scene2d.InputEvent;
import com.badlogic.gdx.scenes.scene2d.InputListener;
import com.badlogic.gdx.scenes.scene2d.Stage;
import com.badlogic.gdx.scenes.scene2d.ui.Button;
import com.badlogic.gdx.scenes.scene2d.ui.Button.ButtonStyle;
import com.badlogic.gdx.scenes.scene2d.ui.Image;
import com.badlogic.gdx.scenes.scene2d.ui.Label;
import com.badlogic.gdx.scenes.scene2d.ui.Label.LabelStyle;
import com.badlogic.gdx.scenes.scene2d.ui.Skin;
import com.badlogic.gdx.scenes.scene2d.ui.TextButton;
import com.badlogic.gdx.scenes.scene2d.ui.Touchpad;
import com.badlogic.gdx.scenes.scene2d.utils.ChangeListener;
import com.badlogic.gdx.utils.Array;
import com.badlogic.gdx.utils.Pool;

public class MainMenuWorld implements MissileCreator {

	private Dogfight game;
	private ResourceFactory resourceFactory;
	private Stage stage;
	private OrthographicCamera camera;
	private SpriteBatch batch;
	private Touchpad touchpad;
	private Button accelerate, fire, achievements, leaderboards;
	private Hero heroPlane;
	private World world;
	private Array<Missile> missileRegistry;
	private Array<Cloud> cloudRegistry;
	private Image title;
	private Texture titleTexture;
	private Label rotate, pedal, fireLabel, creditLabel;
	private TextButton play, credits, back;
	private boolean isCredits;
	private Pool<Cloud> cloudPool;

	public MainMenuWorld(Dogfight g) {
		this.game = g;
		resourceFactory = game.getResourceFactory();
		batch = resourceFactory.getBatch();
		camera = resourceFactory.getCamera();
		stage = resourceFactory.getStage();
		world = new World(new Vector2(0, -2.5f), true);
		titleTexture = new Texture("data/title.png");
		titleTexture.setFilter(TextureFilter.Linear, TextureFilter.Linear);
		LabelStyle ls = resourceFactory.getScoreStyle();
		Skin skin = resourceFactory.getuiSkin();

		heroPlane = new Hero(world, this);

		touchpad = new Touchpad(2, skin);
		fire = new Button(skin.get("fireButton", ButtonStyle.class));
		accelerate = new Button(skin.get("accelerateButton", ButtonStyle.class));
		achievements = new Button(resourceFactory.getuiSkin().get("Ach",
				ButtonStyle.class));
		leaderboards = new Button(resourceFactory.getuiSkin().get("Lea",
				ButtonStyle.class));
		cloudRegistry = new Array<Cloud>(20);
		missileRegistry = new Array<Missile>();
		cloudPool = resourceFactory.getCloudPool();
		title = new Image(titleTexture);
		rotate = new Label("Rotate", ls);
		pedal = new Label("Accelerate", ls);
		fireLabel = new Label("Fire", ls);
		play = new TextButton("Play", skin);
		credits = new TextButton("Credits", skin);
		back = new TextButton("Back", skin);
		creditLabel = new Label(
				"Game made possible by the libgdx engine"
						+ gcNiklas()
						+ "\nArt by Justen R. Moore: Luneder art"
						+ "\nUI graphics by Kenney.nl"
						+ "\n\nThis game \"Dogfight Gauntlet\" uses these sounds from freesound:"
						+ "\nError.wav by Autistic Lucario (http://www.freesound.org/people/Autistic%20Lucario/)"
						+ "\nscary.mp3 by Kastenfrosch (http://www.freesound.org/people/Kastenfrosch/)"
						+ "\nShocked gasp.wav by GentlemanWalrus (http://www.freesound.org/people/GentlemanWalrus/)"
						+ "\nscanner beep.wav by kalisemorrison (http://www.freesound.org/people/kalisemorrison/)"
						+ "\nsynth-cricket.wav by guitarguy1985 (http://www.freesound.org/people/guitarguy1985/)"
						+ "\nlevel_up_3note2.wav by jivatma07 (http://www.freesound.org/people/jivatma07/)"
						+ "\nIcons created by Android Asset Studio is licensed \nunder a Creative Commons Attribution 3.0 Unported License."
						+ "\nhttp://android-ui-utils.googlecode.com/hg/asset-studio/dist/index.html"
						+ "\ntheme_of_the_defenders by Eric Taylors under CC BY 4.0"
						+ "\nsad_Exploring by Eric Taylors under CC BY 4.0"
						+ "\nEric Taylors can be found at http://erictaylormusic.com/",
				ls);

		touchpad.addListener(new ChangeListener() {
			float rotation;

			@Override
			public void changed(ChangeEvent event, Actor actor) {
				if (actor.equals(touchpad)) {
					rotation = MathUtils.atan2(touchpad.getKnobPercentY(),
							touchpad.getKnobPercentX());
					while (rotation < 0)
						rotation += 6;

					if (touchpad.isTouched())
						heroPlane.setBox2dAngle(rotation);
				}
			}
		});
		fire.addListener(new InputListener() {
			@Override
			public boolean touchDown(InputEvent event, float x, float y,
					int pointer, int button) {
				return true;
			}

			@Override
			public void touchUp(InputEvent event, float x, float y,
					int pointer, int button) {
				heroPlane.fireMissile();
			}
		});

		accelerate.addListener(new InputListener() {
			@Override
			public boolean touchDown(InputEvent event, float x, float y,
					int pointer, int button) {
				if (!accelerate.isChecked()) {
					pedal.setText("Stop");
					pedal.setX(1900 + pedal.getPrefWidth() / 2);
				} else {
					pedal.setText("Accelerate");
					pedal.setX(1900);
				}
				return true;
			}
		});

		play.addListener(new InputListener() {
			@Override
			public boolean touchDown(InputEvent event, float x, float y,
					int pointer, int button) {
				return true;
			}

			@Override
			public void touchUp(InputEvent event, float x, float y,
					int pointer, int button) {
				game.chooseScreens(2);
			}
		});

		credits.addListener(new InputListener() {
			@Override
			public boolean touchDown(InputEvent event, float x, float y,
					int pointer, int button) {
				return true;
			}

			@Override
			public void touchUp(InputEvent event, float x, float y,
					int pointer, int button) {
				stage.clear();
				stage.addActor(creditLabel);
				stage.addActor(back);
				isCredits = true;
			}
		});

		back.addListener(new InputListener() {
			@Override
			public boolean touchDown(InputEvent event, float x, float y,
					int pointer, int button) {
				return true;
			}

			@Override
			public void touchUp(InputEvent event, float x, float y,
					int pointer, int button) {
				init();
				isCredits = false;
			}
		});

		achievements.addListener(new InputListener() {
			@Override
			public boolean touchDown(InputEvent event, float x, float y,
					int pointer, int button) {
				return true;
			}

			@Override
			public void touchUp(InputEvent event, float x, float y,
					int pointer, int button) {
				game.showAchievements();
			}
		});

		leaderboards.addListener(new InputListener() {
			@Override
			public boolean touchDown(InputEvent event, float x, float y,
					int pointer, int button) {
				return true;
			}

			@Override
			public void touchUp(InputEvent event, float x, float y,
					int pointer, int button) {
				game.showLeaderBoards();
			}
		});

		touchpad.setPosition(150, 50);
		accelerate.setPosition(1900, 100);
		fire.setPosition(2200, 100);
		title.setPosition(500, 900);
		play.setPosition(1250 - play.getPrefWidth() / 2, 650);
		credits.setPosition(1250 - credits.getPrefWidth() / 2,
				650 - (play.getPrefHeight() + 40));

		fireLabel.setPosition(2200 + fireLabel.getPrefWidth() / 2,
				150 + fire.getPrefHeight());
		pedal.setPosition(1900 + pedal.getPrefWidth() / 2, fireLabel.getY());
		rotate.setPosition(130 + rotate.getPrefWidth() / 2, fireLabel.getY());
		creditLabel.setBounds(150, 650, 1700, 600);
		back.setPosition(150, 150);

		achievements.setPosition(100, 1400 - achievements.getPrefHeight());
		leaderboards.setPosition(2400 - leaderboards.getPrefWidth(),
				1400 - achievements.getPrefHeight());

	}

	// Populate clouds

	public void init() {
		stage.clear();
		pedal.setText("Stop");
		pedal.setX(1900 + pedal.getPrefWidth() / 2);
		accelerate.setChecked(true);
		stage.addActor(touchpad);
		stage.addActor(accelerate);
		stage.addActor(fire);
		stage.addActor(title);
		stage.addActor(fireLabel);
		stage.addActor(pedal);
		stage.addActor(rotate);
		stage.addActor(play);
		stage.addActor(credits);
		stage.addActor(achievements);
		stage.addActor(leaderboards);

	}

	public void update(float delta) {

		populateClouds();
		batch.begin();
		if (!isCredits) {
			float x, y, distance;
			x = heroPlane.getX();
			y = heroPlane.getY();
			heroPlane.update(accelerate.isChecked(), delta);
			// camera.position.set(x + 64, y - 332, 0);
			camera.position.set(x + 64, y - 232, 0);
			camera.update();
			batch.setProjectionMatrix(camera.combined);
			// Move the box2d and update the Image
			// Hud repositioning

			// // Populate World with clouds
			for (Cloud cloud : cloudRegistry) {
				cloud.update(delta);
				float cloudX = cloud.getX(), cloudY = cloud.getY();
				// Is the cloud still on screen
				cloudX = cloudX - x;
				cloudY = cloudY - y;
				distance = ResourceFactory.distance(cloudX, cloudY);
				// half width and height b/c plane is in the middle of ship
				if (distance > 1724)
					cloud.updateTimeNotSeen(delta);
				else
					cloud.updateTimeNotSeen(-1);

				if (cloud.Overdue()) {
					cloudRegistry.removeValue(cloud, false);
					cloudPool.free(cloud);
				} else
					cloud.draw(batch);

			}

			for (Missile missile : missileRegistry) {
				float missileX = missile.getX(), missileY = missile.getY();
				missile.update(delta);
				// Is the missile still on screen
				missileX = missileX - x;
				missileY = missileY - y;
				distance = ResourceFactory.distance(missileX, missileY);
				// half width and height b/c plane is in the middle of ship
				if (distance > 1724)
					missile.updateTimeNotSeen(delta);
				else
					missile.updateTimeNotSeen(-1);

				if (missile.Overdue()) {
					world.destroyBody(missile.getBody());// Destroy body
					missileRegistry.removeValue(missile, false);
					Gdx.app.debug("Missile Registry", missileRegistry.size + "");
					// Remove from registry
				} else
					missile.draw(batch);
			}

			heroPlane.draw(batch);

			world.step(1 / 45f, 6, 2);
		} else {

			for (Cloud cloud : cloudRegistry) {
				float cloudDistance;
				cloud.update(delta);
				float cloudX = cloud.getX(), cloudY = cloud.getY();
				// Is the cloud still on screen
				cloudX = heroPlane.getX() - cloudX;
				cloudY = heroPlane.getY() - cloudY;
				cloudDistance = ResourceFactory.distance(cloudX, cloudY);
				// half width and height b/c plane is in the middle of ship
				if (cloudDistance > 2000)
					cloud.updateTimeNotSeen(delta);
				else
					cloud.updateTimeNotSeen(-1);

				if (cloud.Overdue()) {
					cloudRegistry.removeValue(cloud, false);
					cloudPool.free(cloud);
				} else
					cloud.draw(batch);
			}

		}
		batch.end();
		stage.act();
		stage.draw();

	}

	private void populateClouds() {

		float chance = (20 - cloudRegistry.size) / 10;
		float x, y;
		x = heroPlane.getX();
		y = heroPlane.getY();

		if (MathUtils.randomBoolean(chance)) {
			Cloud cloud = cloudPool.obtain();
			cloud.init();
			switch (MathUtils.random(3)) {
			case 0: // up
				cloud.setPosition(MathUtils.random(x - 1500, x + 1500),
						MathUtils.random(y + 1000, y + 1800));
				break;
			case 1: // down
				cloud.setPosition(MathUtils.random(x - 1500, x + 1500),
						MathUtils.random(y - 1800, y - 1000));
				break;
			case 2: // left
				cloud.setPosition(MathUtils.random(x - 2500, x - 1500),
						MathUtils.random(y - 1000, y + 1000));
			case 3: // right
				cloud.setPosition(MathUtils.random(x + 1500, x + 2500),
						MathUtils.random(y - 1000, y + 1000));

			}
			cloudRegistry.add(cloud);
		}

	}

	public void resize(int width, int height) {
		stage.getViewport().update(width, height, true);
	}

	@Override
	public void createMissile(Plane plane) {
		float rotation = plane.getRotation(), radian, x, y;
		Missile missile = new Missile(world, plane.returnGroupIndex());
		if ((rotation <= 45 || rotation >= 315)) {
			radian = rotation * MathUtils.degreesToRadians;
			x = plane.getX() + 40;
			y = plane.getY();

			// r cos t
			x = x + 15 * MathUtils.cos(radian);
			y = y + 10 * MathUtils.sin(radian);

			x = (plane.getX() + 40) + 25 * MathUtils.cos(radian);
			y = (plane.getY()) + 25 * MathUtils.sin(radian);
			missile.initialPositon(x, y, rotation);

		} else if (rotation > 45 && rotation < 135) {
			radian = rotation * MathUtils.degreesToRadians;
			x = plane.getX() + 32;
			y = plane.getY() + 64;

			// r cos t
			x = x + 67 * MathUtils.cos(radian);
			y = y + 32 * MathUtils.sin(radian);
			missile.initialPositon(x, y, rotation);
		} else if (rotation > 135 && rotation < 225) {

			radian = rotation * MathUtils.degreesToRadians;
			x = plane.getX();
			y = plane.getY();

			// r cos t

			x = x + 25 * MathUtils.cos(radian);
			y = y + 25 * MathUtils.sin(radian);
			missile.initialPositon(x, y, rotation);

		} else if (rotation > 225 && rotation < 315) {

			radian = rotation * MathUtils.degreesToRadians;
			x = plane.getX() + 32;
			y = plane.getY() - 64;

			// r cos t
			x = x + 67 * MathUtils.cos(radian);
			y = y - 32 * MathUtils.sin(radian);
			missile.initialPositon(x, y, rotation);

		}
		missileRegistry.add(missile);

	}

	public String gcNiklas() {
		if (Gdx.app.getType() != ApplicationType.iOS)
			return "";
		else
			return "\niOS version made possible by Niklas Therning and theRobovm Team -- robovm.org"
					+ "\nAdColony custom bindings by HD_92 on badlogic forums"
					+ "\nGame Center custom bindings by HD_92 and YaW on badlogic forums";
	}

	public void dispose() {
		titleTexture.dispose();
	}

	public void freeCloudPool() {
		cloudPool.freeAll(cloudRegistry);
		cloudRegistry.clear();
	}

}
