package com.Fawaz.Dogfight.Model;

import com.Fawaz.Dogfight.Utils.Disposable;
import com.Fawaz.Dogfight.Utils.MissileCreator;
import com.Fawaz.Dogfight.Utils.ResourceFactory;
import com.badlogic.gdx.graphics.g2d.Animation;
import com.badlogic.gdx.graphics.g2d.Animation.PlayMode;
import com.badlogic.gdx.graphics.g2d.Sprite;
import com.badlogic.gdx.graphics.g2d.TextureAtlas;
import com.badlogic.gdx.graphics.g2d.TextureRegion;
import com.badlogic.gdx.math.MathUtils;
import com.badlogic.gdx.math.Vector2;
import com.badlogic.gdx.physics.box2d.Body;
import com.badlogic.gdx.physics.box2d.BodyDef;
import com.badlogic.gdx.physics.box2d.BodyDef.BodyType;
import com.badlogic.gdx.physics.box2d.FixtureDef;
import com.badlogic.gdx.physics.box2d.PolygonShape;
import com.badlogic.gdx.physics.box2d.World;

public abstract class Plane extends Sprite implements Disposable {

	protected TextureAtlas atlas;
	protected Body body;
	protected float upStateTime = 1, rightStateTime = 1, speed;
	protected Animation upAnimation, rightAnimation;
	private Vector2 speedVector;
	private short groupIndex;
	private MissileCreator missileCreator;

	// private Animation up_down, right_left;

	public Plane(World world, TextureAtlas atlas, short groupIndex,
			MissileCreator missileCreator) {
		super();
		upAnimation = new Animation(.1f, atlas.findRegions("up"), PlayMode.LOOP);
		rightAnimation = new Animation(.1f, atlas.findRegions("right"),
				PlayMode.LOOP);
		BodyDef polyDef = new BodyDef();
		PolygonShape polyShape = new PolygonShape();
		FixtureDef polyFixture = new FixtureDef();
		speedVector = new Vector2();
		this.atlas = atlas;
		this.groupIndex = groupIndex;
		this.missileCreator = missileCreator;

		polyDef.type = BodyType.DynamicBody;
		// polyDef.position.set(55, 53);
		polyDef.angularDamping = .9f;
		polyDef.linearDamping = .5f;
		body = world.createBody(polyDef);
		polyShape.setAsBox(1, .5f);
		polyFixture.shape = polyShape;
		polyFixture.density = 1;
		polyFixture.friction = 1;
		polyFixture.restitution = 0;
		polyFixture.filter.groupIndex = groupIndex;
		body.createFixture(polyFixture);
		body.setUserData(this);
		setSize(128, 64);
		setOriginCenter();
		polyShape.dispose();

	}

	public void accelerate(float delta) { // set angle first
		float X, Y;
		X = (float) (speed * Math.cos(body.getAngle())) * delta;
		Y = (float) (speed * Math.sin(body.getAngle())) * delta;

		float velChangeX = X - body.getLinearVelocity().x;
		float velChangeY = Y - body.getLinearVelocity().y;
		float impulseX = body.getMass() * velChangeX;
		float impulseY = body.getMass() * velChangeY;
		speedVector.set(impulseX, impulseY);
		body.applyLinearImpulse(speedVector, body.getWorldCenter(), true);
	}

	public void update(float delta) {

		TextureRegion region;
		float rotation = body.getAngle() * MathUtils.radiansToDegrees;
		while (rotation < 0)
			rotation += 360;
		while (rotation > 360)
			rotation -= 360;
		setRotation(rotation);
		if (rotation <= 45 || rotation >= 315) {
			rightStateTime += delta;
			region = rightAnimation.getKeyFrame(rightStateTime);
			if (region.isFlipY())
				region.flip(false, true);

			setRegion(region);
		} else if ((rotation > 45 && rotation < 135)
				|| (rotation > 225 && rotation < 315)) {
			upStateTime += delta;
			region = upAnimation.getKeyFrame(upStateTime);
			setRegion(region);

		} else if (rotation >= 135 && rotation <= 225) {
			rightStateTime += delta;
			region = rightAnimation.getKeyFrame(rightStateTime);

			if (!region.isFlipY())
				region.flip(false, true);
			setRegion(region);

		}
		setPosition(body.getPosition().x * ResourceFactory.BOXTOWORLD,
				body.getPosition().y * ResourceFactory.BOXTOWORLD);
	}

	public void setBox2dAngle(float radian) {

		float nextAngle = (float) (body.getAngle() + body.getAngularVelocity());
		float totalRotation = radian - nextAngle;
		while (totalRotation < -180 * MathUtils.degreesToRadians)
			totalRotation += 360 * MathUtils.degreesToRadians;
		while (totalRotation > 180 * MathUtils.degreesToRadians)
			totalRotation -= 360 * MathUtils.degreesToRadians;
		float desiredAngularVelocity = totalRotation * 60;
		float change = 30 * MathUtils.degreesToRadians;
		desiredAngularVelocity = Math.min(change,
				Math.max(-change, desiredAngularVelocity));
		float impulse = body.getInertia() * desiredAngularVelocity;
		body.applyAngularImpulse(impulse, true);
		// Gdx.app.debug("Plane", "Changing angle: " + radian);
	}

	public short returnGroupIndex() {
		return groupIndex;
	}

	public void fireMissile() {
		missileCreator.createMissile(this);
	}

	@Override
	public Body getBody() {
		// TODO Auto-generated method stub
		return body;
	}
}
