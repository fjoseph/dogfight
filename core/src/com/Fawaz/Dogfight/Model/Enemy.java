package com.Fawaz.Dogfight.Model;

import com.Fawaz.Dogfight.Utils.MissileCreator;
import com.Fawaz.Dogfight.Utils.ResourceFactory;
import com.badlogic.gdx.graphics.g2d.TextureAtlas;
import com.badlogic.gdx.math.MathUtils;
import com.badlogic.gdx.physics.box2d.World;

public class Enemy extends Plane {

	private float distance = 1119, timeToReplenish, timeNotSeen, cooldown;
	private int score;

	public Enemy(World world, TextureAtlas atlas, MissileCreator missileCreator) {
		super(world, atlas, (short) -3, missileCreator);
		cooldown = 0;

	}

	public void setLevel(int level) {
		switch (level) {
		case 0:
			timeToReplenish = 4;
			speed = 200;
			distance = 800;
			score = 5;
			break;
		case 1:
			timeToReplenish = 4;
			speed = 220;
			distance = 900;
			score = 10;
			break;
		case 2:
			timeToReplenish = 3;
			speed = 240;
			distance = 900;
			score = 20;
			break;
		case 3:
			timeToReplenish = 2;
			speed = 260;
			distance = 950;
			score = 40;
			break;
		case 4:
			timeToReplenish = 1;
			speed = 270;
			distance = 1119;
			score = 80;
			break;
		}
	}

	public void updateTimeNotSeen(float timeNotSeen) {
		if (timeNotSeen == -1)
			this.timeNotSeen = 0;
		else
			this.timeNotSeen += timeNotSeen;
		// Gdx.app.debug("Missile", "Time not seen: " + this.timeNotSeen);
	}

	public boolean Overdue() {
		return timeNotSeen >= 4;
	}

	@Override
	public void fireMissile() {
		// Find the distance before firing
		if (cooldown >= timeToReplenish) {
			super.fireMissile();
			cooldown = 0;
		}
	}

	public void update(float delta) {
		// TODO Auto-generated method stub
		cooldown += delta;
		super.update(delta);
	}

	public float getBox2dAngle() {
		return body.getAngle();
	}

	public void decisionToFire(float distance) {
		if (this.distance >= distance) {
			fireMissile();
		}
	}

	public void setBearing(float x, float y, float angleInDegrees) {
		setPosition(x, y);
		setRotation(angleInDegrees);
		// Scaling
		body.setTransform(x * ResourceFactory.WORLDTOBOX, y
				* ResourceFactory.WORLDTOBOX, angleInDegrees
				* MathUtils.degreesToRadians);
		
	}

	public int getScore() {
		return score;
	}

}
