package com.Fawaz.Dogfight.Model;

import com.Fawaz.Dogfight.Utils.MissileCreator;
import com.Fawaz.Dogfight.Utils.ResourceFactory;
import com.badlogic.gdx.graphics.g2d.TextureAtlas;
import com.badlogic.gdx.graphics.g2d.TextureRegion;
import com.badlogic.gdx.math.MathUtils;
import com.badlogic.gdx.physics.box2d.World;

public class Hero extends Plane {

	private static TextureAtlas heroAtlas = new TextureAtlas("data/Hero.pack");

	public Hero(World world, MissileCreator missileCreator) {
		super(world, heroAtlas, (short) -2, missileCreator);
		this.speed = 350;
		body.setTransform(3.05f, 3.05f, 0);
		setPosition(body.getPosition().x * ResourceFactory.BOXTOWORLD,
				body.getPosition().y * ResourceFactory.BOXTOWORLD);
	}

	public void update(boolean isChecked, float delta) {
		if (isChecked)
			accelerate(delta);

		TextureRegion region = null;
		float rotation = body.getAngle() * MathUtils.radiansToDegrees;

		while (rotation < 0)
			rotation += 360;
		while (rotation > 360)
			rotation -= 360;
		setRotation(rotation);
		if (rotation <= 45 || rotation >= 315) {
			rightStateTime += delta;
			if (isChecked)
				region = rightAnimation.getKeyFrame(rightStateTime);
			else
				region = atlas.findRegion("right_neutral");

			if (region.isFlipY())
				region.flip(false, true);

			setRegion(region);
		} else if ((rotation > 45 && rotation < 135)
				|| (rotation > 225 && rotation < 315)) {
			upStateTime += delta;
			if (isChecked)
				region = upAnimation.getKeyFrame(upStateTime);
			else
				region = atlas.findRegion("up_neutral");
			setRegion(region);

		} else if (rotation >= 135 && rotation <= 225) {
			rightStateTime += delta;
			if (isChecked)
				region = rightAnimation.getKeyFrame(rightStateTime);
			else
				region = atlas.findRegion("right_neutral");

			if (!region.isFlipY())
				region.flip(false, true);
			setRegion(region);

		}
		setPosition(body.getPosition().x * ResourceFactory.BOXTOWORLD,
				body.getPosition().y * ResourceFactory.BOXTOWORLD);
	}

	public static void dispose() {
		heroAtlas.dispose();
	}

}
