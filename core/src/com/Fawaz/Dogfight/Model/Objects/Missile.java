package com.Fawaz.Dogfight.Model.Objects;

import com.Fawaz.Dogfight.Utils.Disposable;
import com.Fawaz.Dogfight.Utils.ResourceFactory;
import com.badlogic.gdx.graphics.g2d.Animation;
import com.badlogic.gdx.graphics.g2d.Sprite;
import com.badlogic.gdx.graphics.g2d.TextureAtlas.AtlasRegion;
import com.badlogic.gdx.math.MathUtils;
import com.badlogic.gdx.math.Vector2;
import com.badlogic.gdx.physics.box2d.Body;
import com.badlogic.gdx.physics.box2d.BodyDef;
import com.badlogic.gdx.physics.box2d.BodyDef.BodyType;
import com.badlogic.gdx.physics.box2d.FixtureDef;
import com.badlogic.gdx.physics.box2d.PolygonShape;
import com.badlogic.gdx.physics.box2d.World;
import com.badlogic.gdx.utils.Array;

public class Missile extends Sprite implements Disposable {

	private Animation animation;
	private Body body;
	private float stateTime = 0, timeNotSeen = 0, speed = 500;
	private Vector2 speedVector;
	public static Array <AtlasRegion> frames;

	public Missile( World world, short groupIndex) {
		super();
		animation = new Animation(.1f, frames);
		BodyDef polyDef = new BodyDef();
		PolygonShape polyShape = new PolygonShape();
		FixtureDef polyFixture = new FixtureDef();
		speedVector = new Vector2();

		setRegion(animation.getKeyFrame(0));
		setSize(64, 64);
		setOriginCenter();

		polyDef.type = BodyType.DynamicBody;
		polyDef.bullet = true;
		polyDef.angularDamping = .9f;
		polyDef.linearDamping = .5f;
		body = world.createBody(polyDef);
		polyShape.setAsBox(.5f, .5f);
		polyFixture.shape = polyShape;
		polyFixture.density = 1;
		polyFixture.friction = 1;
		polyFixture.restitution = 0;
		polyFixture.filter.groupIndex = groupIndex;
		body.createFixture(polyFixture);
		body.setUserData(this);
		
		polyShape.dispose();
	}

	public void update(float delta) {
		stateTime += delta;
		setRegion(animation.getKeyFrame(stateTime, true));
		// set angle first
		float X, Y;
		X = (float) (speed * Math.cos(body.getAngle())) * delta;
		Y = (float) (speed * Math.sin(body.getAngle())) * delta;

		float velChangeX = X - body.getLinearVelocity().x;
		float velChangeY = Y - body.getLinearVelocity().y;
		float impulseX = body.getMass() * velChangeX;
		float impulseY = body.getMass() * velChangeY;

		speedVector.set(impulseX, impulseY);
		body.applyLinearImpulse(speedVector, body.getWorldCenter(), true);
		setPosition(body.getPosition().x * ResourceFactory.BOXTOWORLD,
				body.getPosition().y * ResourceFactory.BOXTOWORLD);
		setRotation(body.getAngle() * MathUtils.radiansToDegrees);

	}

	public void updateTimeNotSeen(float timeNotSeen) {
		if (timeNotSeen == -1)
			this.timeNotSeen = 0;
		else
			this.timeNotSeen += timeNotSeen;
		// Gdx.app.debug("Missile", "Time not seen: " + this.timeNotSeen);
	}

	public boolean Overdue() {
		return timeNotSeen >= 1;
	}

	public void initialPositon(float x, float y, float angleInDegrees) {
		setPosition(x, y);
		setRotation(angleInDegrees);
		body.setTransform(new Vector2(x * ResourceFactory.WORLDTOBOX, y
				* ResourceFactory.WORLDTOBOX), angleInDegrees
				* MathUtils.degreesToRadians);
	}

	public Body returnBody() {
		return body;
	}

	@Override
	public Body getBody() {
		// TODO Auto-generated method stub
		return body;
	}
}
