package com.Fawaz.Dogfight.Model.Objects;

import static com.badlogic.gdx.scenes.scene2d.actions.Actions.delay;
import static com.badlogic.gdx.scenes.scene2d.actions.Actions.moveTo;
import static com.badlogic.gdx.scenes.scene2d.actions.Actions.parallel;
import static com.badlogic.gdx.scenes.scene2d.actions.Actions.run;
import static com.badlogic.gdx.scenes.scene2d.actions.Actions.sequence;
import static com.badlogic.gdx.scenes.scene2d.actions.Actions.sizeTo;

import java.text.DecimalFormat;

import com.Fawaz.Dogfight.Model.Objects.MicroGames.MicroGame;
import com.Fawaz.Dogfight.Utils.ResourceFactory;
import com.badlogic.gdx.scenes.scene2d.Group;
import com.badlogic.gdx.scenes.scene2d.ui.Image;
import com.badlogic.gdx.scenes.scene2d.ui.Label;
import com.badlogic.gdx.scenes.scene2d.ui.Label.LabelStyle;
import com.badlogic.gdx.scenes.scene2d.ui.Skin;
import com.badlogic.gdx.scenes.scene2d.ui.TextButton;

public class MicroGameWindow extends Group {

	private Image background;
	private MicroGame currentMG;
	private Label title, score, best, timeAlive, bestTimeAlive, highestCombo,
			bestHighestCombo;
	private TextButton restart, mainMenu;
	private DecimalFormat df;

	public MicroGameWindow(ResourceFactory resourceFactory) {
		setSize(2000, 1000);
		Skin dialogSkin = resourceFactory.getDialogSkin();
		background = new Image(dialogSkin.getDrawable("dWindow"));
		title = new Label("", dialogSkin.get("MicroGame Title",
				LabelStyle.class));
		restart = new TextButton("Restart", dialogSkin);
		mainMenu = new TextButton("Main Menu", dialogSkin);
		score = new Label("", resourceFactory.getScoreStyle());
		best = new Label("", resourceFactory.getScoreStyle());
		timeAlive = new Label("", resourceFactory.getScoreStyle());
		bestTimeAlive = new Label("", resourceFactory.getScoreStyle());
		highestCombo = new Label("", resourceFactory.getScoreStyle());
		bestHighestCombo = new Label("", resourceFactory.getScoreStyle());
		df = new DecimalFormat();

		df.setMaximumFractionDigits(2);
		df.setMinimumFractionDigits(2);

		addActor(background);
	}

	public void setButtons(TextButton restart, TextButton mainMenu) {
		this.restart = restart;
		this.mainMenu = mainMenu;
	}

	public void removeActors() {
		score.remove();
		best.remove();
		timeAlive.remove();
		bestTimeAlive.remove();
		highestCombo.remove();
		bestHighestCombo.remove();
		restart.remove();
		mainMenu.remove();
	}

	public void initializeMicroGame(MicroGame mg) {

		removeActors();
		setBounds(810, 464, 880, 573);
		currentMG = mg;
		background.setSize(880, 573);
		title.setText(currentMG.getName());
		title.setPosition(440 - title.getPrefWidth() * .5f,
				595 - title.getPrefHeight());
		background.addAction(delay(.5f, sizeTo(2000, 1000, .5f)));
		addAction(delay(
				.5f,
				sequence(
						parallel(moveTo(250, 250, .5f), sizeTo(2000, 1000, .5f)),
						run(new Runnable() {

							@Override
							public void run() {
								addActor(currentMG);
								currentMG.init();
							}
						}))));

		title.addAction(delay(
				.5f,
				moveTo(1000 - (title.getPrefWidth() * .5f),
						1000 - title.getPrefHeight(), .5f)));
		addActor(title);

	}

	public void initializeScoreList(int finalScore, int BestScore,
			float timeAliveNum, float BestAliveNum, int highestComboNum,
			int bestHighestComboNum, boolean isFromGGame) {
		removeActors();
		title.setText("Game Over");
		if (finalScore < BestScore) {
			score.setText("Score \n\nScore: " + finalScore);
			best.setText("High Score \n\nScore: " + BestScore);
		} else {
			score.setText("New High Score\n\nScore: " + finalScore);
			best.setText("Old High Score \n\nScore: " + BestScore);
		}
		timeAlive.setText("Time: " + df.format(timeAliveNum) + "s");
		bestTimeAlive.setText("Time: " + df.format(BestAliveNum) + "s");
		highestCombo.setText("Highest Combo: x" + highestComboNum);
		bestHighestCombo.setText("Highest Combo: x" + bestHighestComboNum);

		if (isFromGGame) {
			setBounds(810, 464, 880, 573);
			title.setPosition(440 - title.getPrefWidth() * .5f,
					595 - title.getPrefHeight());
			background.setSize(880, 573);
			background.addAction(delay(.5f, sizeTo(2000, 1000, .5f)));
			addAction(delay(
					.5f,
					sequence(
							parallel(moveTo(250, 250, .5f),
									sizeTo(2000, 1000, .5f)),
							run(new Runnable() {

								@Override
								public void run() {
									addActor(score);
									addActor(best);
									addActor(timeAlive);
									addActor(bestTimeAlive);
									addActor(highestCombo);
									addActor(bestHighestCombo);
									addActor(restart);
									addActor(mainMenu);
								}
							}))));

			title.addAction(delay(
					.5f,
					moveTo(1000 - (title.getPrefWidth() * .5f),
							1000 - title.getPrefHeight(), .5f)));
		} else {
			title.setPosition(1000 - (title.getPrefWidth() * .5f),
					1000 - title.getPrefHeight());

			addActor(score);
			addActor(best);
			addActor(timeAlive);
			addActor(bestTimeAlive);
			addActor(highestCombo);
			addActor(bestHighestCombo);
			addActor(restart);
			addActor(mainMenu);
		}
		score.setPosition(300, 650);
		best.setPosition(1100, 650);
		timeAlive.setPosition(300, score.getY() - 135);
		bestTimeAlive.setPosition(1100, best.getY() - 135);
		highestCombo.setPosition(300, timeAlive.getY() - 90);
		bestHighestCombo.setPosition(1100, bestTimeAlive.getY() - 90);
		restart.setPosition(1150, 100);
		mainMenu.setPosition(350, 100);

		addActor(title);
		ResourceFactory.playGameOverMusic(true);
	}
}
