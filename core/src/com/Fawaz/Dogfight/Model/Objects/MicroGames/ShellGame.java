package com.Fawaz.Dogfight.Model.Objects.MicroGames;

import static com.badlogic.gdx.scenes.scene2d.actions.Actions.alpha;
import static com.badlogic.gdx.scenes.scene2d.actions.Actions.moveTo;
import static com.badlogic.gdx.scenes.scene2d.actions.Actions.sequence;

import com.Fawaz.Dogfight.Utils.ResourceFactory;
import com.Fawaz.Dogfight.View.GameWorld;
import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.TextureAtlas;
import com.badlogic.gdx.math.MathUtils;
import com.badlogic.gdx.scenes.scene2d.InputEvent;
import com.badlogic.gdx.scenes.scene2d.InputListener;
import com.badlogic.gdx.scenes.scene2d.Touchable;
import com.badlogic.gdx.scenes.scene2d.ui.Image;
import com.badlogic.gdx.utils.Array;
import com.badlogic.gdx.utils.Timer;
import com.badlogic.gdx.utils.Timer.Task;

public class ShellGame extends MicroGame {

	private enum Positions {
		FIRST(250), SECOND(900), THIRD(1600);
		// /666 1333
		float x;
		float y = 150;

		Positions(float x) {
			this.x = x;
		}

		public float getX() {
			return x;
		}

		public float getY() {
			return y;
		}
	}

	private Array<Color> colorArray;
	private Array<Image> cupArray;
	private Image neo, coverCup;
	private Task switchTask, pickCupTask, finalTask;
	private boolean won;

	public ShellGame(GameWorld modelWorld, Texture circle) {
		super(modelWorld);
		cupArray = new Array<Image>();
		colorArray = new Array<Color>();

		for (int i = 0; i < 3; i++) {
			cupArray.add(new Image(circle));
		}

		for (Image i : cupArray) {
			i.setSize(200, 200);
			addActor(i);
			i.addListener(new ShellButtonListener(i));
		}

		neo = new Image(
				new TextureAtlas("data/Hero.pack").findRegion("right_neutral"));
		neo.setSize(100, 100);
		addActor(neo);

		colorArray.add(Color.GREEN);
		colorArray.add(Color.BLUE);
		colorArray.add(Color.YELLOW);
		setName("Shell Game");

		switchTask = new Task() {
			int first, second;

			@Override
			public void run() {
				first = MathUtils.random(2);
				second = first;
				while (first == second) {
					second = MathUtils.random(2);
					switchButtons(first, second);
				}

			}
		};

		pickCupTask = new Task() {

			@Override
			public void run() {
				int index = MathUtils.random(2);
				Positions p = Positions.values()[index];
				neo.addAction(sequence(moveTo(p.x + 50, p.y + 50, .5f),
						alpha(0)));
				coverCup = cupArray.get(index);
				coverCup.setName("neo");
			}
		};

		finalTask = new Task() {

			@Override
			public void run() {
				report(won);
			}
		};
		neo.toBack();

	}

	@Override
	public void init() {

		for (int i = 0; i < 3; i++) {
			Image image = cupArray.get(i);
			Positions p = Positions.values()[i];
			Color c = colorArray.get(i);
			image.setName("");
			image.setColor(c);
			image.setPosition(p.getX(), p.getY());
			image.setTouchable(Touchable.enabled);
		}
		
		neo.setPosition(Positions.SECOND.x + 50, 720);
		Timer.schedule(pickCupTask, .5f);
		Timer.schedule(switchTask, 1.5f, .3f, 10);

	}

	private void finalStage() {

		neo.setPosition(coverCup.getX() + 50, coverCup.getY() + 50);
		neo.addAction(alpha(1));
		for (Image i : cupArray) {
			i.addAction(moveTo(i.getX(), 550, .7f));
		}

		if (won)
			ResourceFactory.playSound(2);
		else
			ResourceFactory.playSound(0);

		Timer.schedule(finalTask, 1.5f);
	}

	private void switchButtons(int f, int s) {
		Image first = cupArray.get(f);
		Image second = cupArray.get(s);

		float u, w, x, y;
		u = first.getX();
		w = first.getY();
		x = second.getX();
		y = second.getY();

		first.addAction(moveTo(x, y, .2f));
		second.addAction(moveTo(u, w, .2f));
		for (Image i : cupArray) {
			i.setColor(colorArray.random());
		}
	}

	private class ShellButtonListener extends InputListener {

		private Image image;

		public ShellButtonListener(Image i) {
			image = i;
		}

		@Override
		public boolean touchDown(InputEvent event, float x, float y,
				int pointer, int button) {
			String name = image.getName();
			if (!switchTask.isScheduled()) {
				won = name.equals("neo");
				finalStage();
				for (Image cup : cupArray) {
					cup.setTouchable(Touchable.disabled);
				}
			}

			return true;
		}
	}

}
