package com.Fawaz.Dogfight.Model.Objects.MicroGames.RPS;

import com.Fawaz.Dogfight.Utils.ResourceFactory;
import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.g2d.TextureAtlas.AtlasRegion;
import com.badlogic.gdx.scenes.scene2d.ui.Image;
import com.badlogic.gdx.scenes.scene2d.utils.TextureRegionDrawable;
import com.badlogic.gdx.utils.Array;

public class RPSPlayer extends Image {
	public int handsIndex;
	private Array<AtlasRegion> handsTexture;
	private TextureRegionDrawable trd;

	public RPSPlayer(Array<AtlasRegion> handsTexture) {
		super();
		trd = new TextureRegionDrawable(handsTexture.first());
		setDrawable(trd);
		setSize(1000, 600);
		handsIndex = -1;
		this.handsTexture = handsTexture;

	}

	public void showHands() {
		Gdx.app.debug("HandsIndex", handsIndex + "");
		trd.setRegion(handsTexture.get(handsIndex));
	}

	public void changeHands(int i) {
		trd.setRegion(handsTexture.get(i));
		ResourceFactory.playSound(3);
	}

}
