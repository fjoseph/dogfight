package com.Fawaz.Dogfight.Model.Objects.MicroGames.RPS;

import com.Fawaz.Dogfight.Model.Objects.MicroGames.MicroGame;
import com.Fawaz.Dogfight.Utils.ResourceFactory;
import com.Fawaz.Dogfight.View.GameWorld;
import com.badlogic.gdx.graphics.g2d.TextureAtlas.AtlasRegion;
import com.badlogic.gdx.math.MathUtils;
import com.badlogic.gdx.scenes.scene2d.InputEvent;
import com.badlogic.gdx.scenes.scene2d.InputListener;
import com.badlogic.gdx.scenes.scene2d.ui.Image;
import com.badlogic.gdx.utils.Array;
import com.badlogic.gdx.utils.Timer;
import com.badlogic.gdx.utils.Timer.Task;

public class RockPaperScissors extends MicroGame {

	private RPSPlayer player, opponent;
	private CountDownTask paperCountDown, scissorsCountDown;
	private Task checkInput, endMatch;
	private Image rock, paper, scissors;

	public RockPaperScissors(GameWorld modelWorld) {
		super(modelWorld);
		player = new RPSPlayer(modelWorld.getMicroGameAsset(1));
		opponent = new RPSPlayer(modelWorld.getMicroGameAsset(2));
		paperCountDown = new CountDownTask(1);
		scissorsCountDown = new CountDownTask(2);
		Array<AtlasRegion> icons = modelWorld.getMicroGameAsset(3);
		rock = new Image(icons.first());
		paper = new Image(icons.get(1));
		scissors = new Image(icons.get(2));
		this.modelWorld = modelWorld;
		initTasks();

		player.setPosition(0, 300);
		opponent.setPosition(1000, 300);
		rock.setBounds(30, 0, 250, 300);
		paper.setBounds(400, 0, 250, 300);
		scissors.setBounds(700, 0, 250, 300);

		rock.addListener(new RPSChooser(0));
		paper.addListener(new RPSChooser(1));
		scissors.addListener(new RPSChooser(2));

		setName("Rock Paper Scissors");

	}

	private void initTasks() {

		checkInput = new Task() {

			@Override
			public void run() {
				rock.remove();
				paper.remove();
				scissors.remove();
				if (player.handsIndex == -1)
					player.handsIndex = MathUtils.random(2);

				opponent.handsIndex = MathUtils.random(2);
				player.showHands();
				opponent.showHands();

			}
		};

		endMatch = new Task() {

			@Override
			public void run() {
				if (player.handsIndex == opponent.handsIndex) {
					init();
					ResourceFactory.playSound(5);
				} else if ((player.handsIndex == 0 && opponent.handsIndex == 2)
						|| (player.handsIndex == 1 && opponent.handsIndex == 0)
						|| (player.handsIndex == 2 && opponent.handsIndex == 1)) {
					report(true);
					ResourceFactory.playSound(2);
				} else {
					report(false);
					ResourceFactory.playSound(0);
				}

			}
		};
	}

	@Override
	public void init() {
		addActor(rock);
		addActor(paper);
		addActor(scissors);
		addActor(player);
		addActor(opponent);
		player.changeHands(0);
		opponent.changeHands(0);
		Timer.schedule(paperCountDown, 1);
		Timer.schedule(scissorsCountDown, 2);
		Timer.schedule(checkInput, 3);
		Timer.schedule(endMatch, 4);

	}


	private class CountDownTask extends Task {

		private int index;

		public CountDownTask(int index) {
			this.index = index;
		}

		@Override
		public void run() {
			player.changeHands(index);
			opponent.changeHands(index);
			if (index == 2)
				ResourceFactory.playSound(4);
		}

	}

	public class RPSChooser extends InputListener {
		private int index;

		public RPSChooser(int index) {
			this.index = index;
		}

		@Override
		public boolean touchDown(InputEvent event, float x, float y,
				int pointer, int button) {
			player.handsIndex = index;
			return true;
		}
	}

}
