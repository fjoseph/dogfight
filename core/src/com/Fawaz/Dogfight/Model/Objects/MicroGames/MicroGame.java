package com.Fawaz.Dogfight.Model.Objects.MicroGames;

import com.Fawaz.Dogfight.Model.Objects.MicroGames.RPS.RockPaperScissors;
import com.Fawaz.Dogfight.View.GameWorld;
import com.badlogic.gdx.scenes.scene2d.Group;

public abstract class MicroGame extends Group {

	protected GameWorld modelWorld;

	public MicroGame(GameWorld modelWorld) {
		this.modelWorld = modelWorld;
	}

	public abstract void init();

	public void report(boolean won) {
		remove();
		if (won) {
			if (this instanceof ShellGame)
				modelWorld.wonGame(0);
			else if (this instanceof RockPaperScissors)
				modelWorld.wonGame(1);

		} else
			modelWorld.initializeScoreScreen(false);
	}
}
