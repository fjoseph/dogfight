package com.Fawaz.Dogfight.Model.Objects;

import com.badlogic.gdx.graphics.g2d.Animation;
import com.badlogic.gdx.graphics.g2d.Sprite;
import com.badlogic.gdx.graphics.g2d.TextureAtlas.AtlasRegion;
import com.badlogic.gdx.utils.Array;

public class Explosion extends Sprite {

	private Animation animation;
	private float stateTime = 0;
	private boolean hero;

	public Explosion(Array<AtlasRegion> frames) {
		super();
		animation = new Animation(.07f, frames);
	}
	
	public void setHero(boolean hero){
		this.hero = hero;
	}

	public boolean update(float delta) {
		stateTime += delta;
		setRegion(animation.getKeyFrame(stateTime));
		return animation.isAnimationFinished(stateTime);
	}

	public boolean isHero() {
		return hero;
	}

}
