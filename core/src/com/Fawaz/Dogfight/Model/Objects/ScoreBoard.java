package com.Fawaz.Dogfight.Model.Objects;

import com.badlogic.gdx.scenes.scene2d.Group;
import com.badlogic.gdx.scenes.scene2d.ui.Label;
import com.badlogic.gdx.scenes.scene2d.ui.Label.LabelStyle;

public class ScoreBoard extends Group {

	private Label score, best;
	private int scoreN, multiplierN, highestMultiplier, bestN;
	private float countDown;

	public ScoreBoard(LabelStyle ls, int bestN) {
		score = new Label("Score: 0", ls);
		best = new Label("Best:", ls);
		setBestScore(bestN);

		score.setFontScale(1.5f);
		best.setFontScale(1.5f);

		addActor(best);
		addActor(score);

		best.setPosition(2420 - best.getPrefWidth(), 0);
		score.setPosition(30, 0);
	}

	public void setBestScore(int score) {
		bestN = score;
		best.setText("Best: " + score);
		best.setX(2420 - best.getPrefWidth());
	}

	public void init() {
		scoreN = 0;
		multiplierN = 1;
		highestMultiplier = 1;
		countDown = 0.0f;
		score.setText("Score: 0");
	}

	public int updateScore(int newScore) {
		int overall = newScore * multiplierN;
		scoreN += overall;
		multiplierN++;
		if (multiplierN > highestMultiplier)
			highestMultiplier = multiplierN;
		countDown = 2.5f;
		score.setText("Score: " + scoreN);
		return overall;
	}

	public void update(float delta) {
		countDown -= delta;
		if (countDown <= 0) {
			countDown = .0f;
			multiplierN = 1;
		}

	}

	@Override
	public float getHeight() {
		// TODO Auto-generated method stub
		return best.getPrefHeight() + 20;
	}

	public int getHighestCombo() {
		return highestMultiplier;
	}

	public int getCombo() {
		return multiplierN - 1;
	}

	public int getScore() {
		return scoreN;
	}

	public int getBest() {
		return bestN;
	}
}
