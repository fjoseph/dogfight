package com.Fawaz.Dogfight.Model.Objects;

import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.Sprite;
import com.badlogic.gdx.math.MathUtils;
import com.badlogic.gdx.utils.Pool.Poolable;

public class Cloud extends Sprite implements Poolable {

	private float timeNotSeen = 0;
	private boolean direction;
	private float range;
	public static Texture texture;

	public Cloud() {
		super(texture);
		setSize(64, 64);

	}

	public void init() {
		setScale(MathUtils.random(5, 7));
		direction = MathUtils.randomBoolean();
		range = MathUtils.random(70, 120);
	}

	public void update(float delta) {
		if (direction)
			translateX(range * delta);
		else
			translateX(-(range * delta));
	}

	public void updateTimeNotSeen(float timeNotSeen) {
		if (timeNotSeen == -1)
			this.timeNotSeen = 0;
		else
			this.timeNotSeen += timeNotSeen;
	}

	public boolean Overdue() {
		return timeNotSeen >= 1;
	}

	public void setDirection(boolean isRight) {
		direction = isRight;
	}

	@Override
	public void reset() {
		range = 0;
		setScale(1);
	}

}
