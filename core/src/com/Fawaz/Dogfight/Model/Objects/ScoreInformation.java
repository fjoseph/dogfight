package com.Fawaz.Dogfight.Model.Objects;

public class ScoreInformation {

	public String score;
	public float x, y;
	public float timeRemaining = 1.5f;

	public ScoreInformation(float x, float y, String updateScore) {
		this.x = x;
		this.y = y;
		score = updateScore;
	}

	public boolean update(float delta) {
		if (score.contains("x"))
			x -= 160 * delta;
		else
			x += 160 * delta;
		y += 160 * delta;
		timeRemaining -= delta;
		if (timeRemaining <= 0)
			return true;
		else
			return false;
	}

}
