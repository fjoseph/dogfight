package com.Fawaz.Dogfight.android;

import android.content.Intent;
import android.os.Bundle;

import com.Fawaz.Dogfight.Dogfight;
import com.Fawaz.Dogfight.Utils.ThirdPartyInterface;
import com.badlogic.gdx.backends.android.AndroidApplication;
import com.badlogic.gdx.backends.android.AndroidApplicationConfiguration;
import com.google.android.gms.games.Games;
import com.google.example.games.basegameutils.GameHelper;
import com.google.example.games.basegameutils.GameHelper.GameHelperListener;
import com.jirbo.adcolony.AdColony;
import com.jirbo.adcolony.AdColonyVideoAd;

public class AndroidLauncher extends AndroidApplication implements
		ThirdPartyInterface, GameHelperListener {

	private GameHelper aHelper;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		AdColony.configure(this, "version:1.0,store:google",
				"app9f0cfc7d02064ee295", "vzb3ab510de2374543b5");
		AndroidApplicationConfiguration config = new AndroidApplicationConfiguration();
		initialize(new Dogfight(this), config);
		aHelper = new GameHelper(this, GameHelper.CLIENT_ALL);
		aHelper.enableDebugLog(true);
		aHelper.setup(this);

	}

	@Override
	public void onStart() {
		super.onStart();
		aHelper.onStart(this);

	}

	@Override
	public void onStop() {
		super.onStop();
		aHelper.onStop();
	}

	@Override
	public void onPause() {
		super.onPause();
		AdColony.pause();
	}

	@Override
	public void onActivityResult(int request, int response, Intent data) {
		super.onActivityResult(request, response, data);
		aHelper.onActivityResult(request, response, data);
	}

	@Override
	public void onResume() {
		super.onResume();
		AdColony.resume(this);
	}

	@Override
	public void playAds() {
		AdColonyVideoAd ad = new AdColonyVideoAd();
		ad.show();

	}

	@Override
	public void showAchievements() {
		if (isSignedIn())
			startActivityForResult(
					Games.Achievements.getAchievementsIntent(aHelper
							.getApiClient()), 105);
	}

	@Override
	public void showLeaderBoards() {

		if (isSignedIn())
			startActivityForResult(
					Games.Leaderboards.getAllLeaderboardsIntent(aHelper
							.getApiClient()), 500);
	}

	@Override
	public void unlockAchievements(int index) {
		String achievementId = null;
		if (isSignedIn()) {
			switch (index) {
			case 1:
				achievementId = getString(R.string.achievement_first_of_many);
				break;
			case 2:
				achievementId = getString(R.string.achievement_1_up);
				break;
			case 3:
				achievementId = getString(R.string.achievement_stubborn);
				break;
			case 4:
				achievementId = getString(R.string.achievement_x_ray_vision);
				break;
			case 5:
				achievementId = getString(R.string.achievement_three_way_standoff);
				break;
			case 6:
				achievementId = getString(R.string.achievement_hat_trick);
				break;
			}

			Games.Achievements.unlock(aHelper.getApiClient(), achievementId);
		}

	}

	@Override
	public void submitScore(int destroyedPlanes, int score) {
		if (isSignedIn()) {
			Games.Leaderboards.submitScore(aHelper.getApiClient(),
					getString(R.string.leaderboard_destroyed_planes),
					destroyedPlanes);
			Games.Leaderboards.submitScore(aHelper.getApiClient(),
					getString(R.string.leaderboard_high_score), score);
		}

	}

	@Override
	public void signIn() {
		try {
			runOnUiThread(new Runnable() {

				// @Override
				public void run() {
					aHelper.beginUserInitiatedSignIn();
				}
			});
		} catch (final Exception ex) {

		}
	}

	@Override
	public boolean isSignedIn() {
		return aHelper.isSignedIn();
	}

	@Override
	public void onSignInFailed() {

	}

	@Override
	public void onSignInSucceeded() {
	}

}
