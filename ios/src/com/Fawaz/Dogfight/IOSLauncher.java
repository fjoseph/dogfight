package com.Fawaz.Dogfight;

import java.util.ArrayList;

import org.robovm.apple.foundation.NSArray;
import org.robovm.apple.foundation.NSAutoreleasePool;
import org.robovm.apple.foundation.NSError;
import org.robovm.apple.foundation.NSString;
import org.robovm.apple.gamekit.GKAchievement;
import org.robovm.apple.gamekit.GKLeaderboard;
import org.robovm.apple.uikit.UIApplication;
import org.robovm.bindings.adcolony.AdColony;
import org.robovm.bindings.adcolony.AdColonyAdDelegate;
import org.robovm.bindings.adcolony.AdColonyDelegate;
import org.robovm.bindings.appirater.Appirater;
import org.robovm.bindings.gamecenter.GameCenterListener;
import org.robovm.bindings.gamecenter.GameCenterManager;

import com.Fawaz.Dogfight.Utils.ResourceFactory;
import com.Fawaz.Dogfight.Utils.ThirdPartyInterface;
import com.badlogic.gdx.backends.iosrobovm.IOSApplication;
import com.badlogic.gdx.backends.iosrobovm.IOSApplicationConfiguration;

public class IOSLauncher extends IOSApplication.Delegate implements
		ThirdPartyInterface, AdColonyDelegate, AdColonyAdDelegate {

	private IOSApplication iosApp;
	private Dogfight game;
	private boolean signedIn = false;
	private GameCenterManager gcManager;
	final static String APP_ID = "app1eb77352ffa341d391";
	final static String ZONE_ID = "vze8fa9ceadd814930a3";
	private String[] achievementID, leaderboardID;

	@Override
	protected IOSApplication createApplication() {
		IOSApplicationConfiguration config = new IOSApplicationConfiguration();
		config.orientationLandscape = true;
		config.orientationPortrait = false;
		game = new Dogfight(this);
		achievementID = new String[] { "FOM", "1_Up", "Stubborn", "Xray",
				"RPS", "HTrick" };
		leaderboardID = new String[] { "D_Planes", "High_Score" };
		ArrayList<NSString> aZones = new ArrayList<NSString>();
		aZones.add(new NSString(ZONE_ID));
		NSArray<NSString> zones = new NSArray<NSString>(aZones);
		AdColony.configure(APP_ID, zones, this, true);
		iosApp = new IOSApplication(game, config);
		return iosApp;
	}

	public static void main(String[] argv) {
		NSAutoreleasePool pool = new NSAutoreleasePool();
		UIApplication.main(argv, null, IOSLauncher.class);
		pool.close();
	}

	private void initGC() {
		gcManager = new GameCenterManager(UIApplication.getSharedApplication()
				.getKeyWindow(), new GameCenterListener() {
			@Override
			public void playerLoginFailed(NSError error) {
				System.out.println("playerLoginFailed. error: " + error);
				System.out.println("playerLoginFailed");
				signedIn = false;
			}

			@Override
			public void playerLoginCompleted() {
				System.out.println("playerLoginCompleted");
				signedIn = true;
				// AppiRaterUtil.app_launched(Gdx.app.getPreferences("Rater"));
				Appirater.appLaunched(true);
			}

			@Override
			public void achievementReportCompleted() {
				System.out.println("achievementReportCompleted");
			}

			@Override
			public void achievementReportFailed(NSError error) {
				System.out.println("achievementReportFailed. error: " + error);
			}

			@Override
			public void achievementsLoadCompleted(
					ArrayList<GKAchievement> achievements) {
				System.out.println("achievementsLoadCompleted: "
						+ achievements.size());
			}

			@Override
			public void achievementsLoadFailed(NSError error) {
				System.out.println("achievementsLoadFailed. error: " + error);
			}

			@Override
			public void achievementsResetCompleted() {
				System.out.println("achievementsResetCompleted");
			}

			@Override
			public void achievementsResetFailed(NSError error) {
				System.out.println("achievementsResetFailed. error: " + error);
			}

			@Override
			public void scoreReportCompleted() {
				System.out.println("scoreReportCompleted");
			}

			@Override
			public void scoreReportFailed(NSError error) {
				System.out.println("scoreReportFailed. error: " + error);
			}

			@Override
			public void leaderboardsLoadCompleted(
					ArrayList<GKLeaderboard> scores) {
				System.out.println("scoresLoadCompleted: " + scores.size());
			}

			@Override
			public void leaderboardsLoadFailed(NSError error) {
				System.out.println("scoresLoadFailed. error: " + error);
			}

			@Override
			public void achievementViewDismissed() {
				// TODO Auto-generated method stub

			}

			@Override
			public void leaderboardViewDismissed() {
				// TODO Auto-generated method stub

			}
		});

		Appirater.setAppId("879829062");
		Appirater.setDaysUntilPrompt(-1);
		Appirater.setUsesUntilPrompt(5);
	}

	@Override
	public void playAds() {
		AdColony.playVideoAd(ZONE_ID, this);
	}

	@Override
	public void unlockAchievements(int index) {
		if (signedIn)
			gcManager.reportAchievement(achievementID[index - 1]);
	}

	@Override
	public void showAchievements() {
		if (signedIn)
			gcManager.showAchievementsView();
	}

	@Override
	public void showLeaderBoards() {
		gcManager.showLeaderboardsView();
	}

	@Override
	public void signIn() {
		initGC();
		gcManager.login();
	}

	@Override
	public void onAdStartedInZone(String zoneID) {
		ResourceFactory.playGameOverMusic(false);

	}

	@Override
	public void onAdAttemptFinished(boolean shown, String zoneID) {
		ResourceFactory.playGameOverMusic(true);

	}

	@Override
	public void onAdAvailabilityChange(boolean available, String zoneID) {

	}

	@Override
	public void onV4VCReward(boolean success, String currencyName, int amount,
			String zoneID) {

	}

	@Override
	public boolean isSignedIn() {
		return signedIn;
	}

	@Override
	public void submitScore(int destroyedPlanes, int score) {

		if (signedIn) {
			gcManager.reportScore(leaderboardID[1], score);
			gcManager.reportScore(leaderboardID[0], destroyedPlanes);
		}
	}
}