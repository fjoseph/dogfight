package com.Fawaz.Dogfight.desktop;

import com.Fawaz.Dogfight.Dogfight;
import com.Fawaz.Dogfight.Utils.ThirdPartyInterface;
import com.badlogic.gdx.backends.lwjgl.LwjglApplication;
import com.badlogic.gdx.backends.lwjgl.LwjglApplicationConfiguration;

public class DesktopLauncher implements ThirdPartyInterface {
	public static void main(String[] arg) {
		LwjglApplicationConfiguration config = new LwjglApplicationConfiguration();
		DesktopLauncher desktop = new DesktopLauncher();

		new LwjglApplication(new Dogfight(desktop), config);
	}

	@Override
	public void playAds() {
		// TODO Auto-generated method stub

	}

	@Override
	public void unlockAchievements(int index) {
		// TODO Auto-generated method stub

	}

	@Override
	public void showAchievements() {
		// TODO Auto-generated method stub

	}

	@Override
	public void showLeaderBoards() {
		// TODO Auto-generated method stub

	}

	@Override
	public void signIn() {
		// TODO Auto-generated method stub

	}

	@Override
	public boolean isSignedIn() {
		// TODO Auto-generated method stub
		return false;
	}

	@Override
	public void submitScore(int index, int score) {
		// TODO Auto-generated method stub

	}
}
